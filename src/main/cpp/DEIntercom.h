//
// Created by Dan on 05.12.2021.
//

#ifndef LIBRARY_DEINTERCOM_DEINTERCOM_H
#define LIBRARY_DEINTERCOM_DEINTERCOM_H

#include <IntMain.h>
#include <IntSchema.h>
#include <IntPtyFs.h>
#include <IntSerial.h>
#include <IntUsbSerial.h>
#include <IntDatabase.h>
#include <IntServer.h>
#include <IntClient.h>
#include <IntApnClient.h>
#include <IntFrbClient.h>
#include <IntIgd.h>
#include <IntPinger.h>
#include <IntJwt.h>
#include <IntSipAgent.h>
#include <IntProgrammer.h>
#include <IntAsrModel.h>
#include <IntAsrRecognizer.h>
#include <IntFFmpeg.h>
#include <IntInit.h>

#endif //LIBRARY_DEINTERCOM_DEINTERCOM_H
