//
// Created by Dan on 24.06.2022.
//

#include "IntApnClient.h"










IntApnClientType IntApnClient = {0};

void IntApnClientLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntApnClient");
    IntApnClient.class = (*env)->NewGlobalRef(env, class);
    IntApnClient.object = (*env)->GetFieldID(env, class, "object", "J");
    IntApnClient.weak = (*env)->GetFieldID(env, class, "weak", "J");
    IntApnClient.base = (*env)->GetFieldID(env, class, "base", "Ljava/lang/String;");
    IntApnClient.credential = (*env)->GetFieldID(env, class, "credential", "Llibrary/apnext/ApnCredential;");
    IntApnClient.listeners = (*env)->GetFieldID(env, class, "listeners", "Llibrary/deintercom/IntApnClient$Listeners;");
    IntApnClient.init = (*env)->GetMethodID(env, class, "<init>", "()V");
    IntApnClient.pushDone = (*env)->GetMethodID(env, class, "pushDone", "(Ljava/lang/String;Llibrary/glibext/GeException;)V");
}

void IntApnClientUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntApnClient.class);
}

JNIEXPORT void JNICALL Java_library_deintercom_IntApnClient_close(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, IntApnClient.object);
    if (object == 0) return;
    g_object_unref(GSIZE_TO_POINTER(object));
    (*env)->SetLongField(env, this, IntApnClient.object, 0);

    jlong weak = (*env)->GetLongField(env, this, IntApnClient.weak);
    (*env)->DeleteWeakGlobalRef(env, GSIZE_TO_POINTER(weak));
}

void IntApnClientINTAPNMultiPushDone(INTAPNMulti *sdkSender, gchar *sdkId, GError *sdkError, gpointer sdkThis) {
    JNIEnv *env = NULL;
    if ((*GeJavaVM)->AttachCurrentThread(GeJavaVM, &env, NULL) < JNI_OK) return;

    if (sdkId == NULL) {
        jthrowable exception = GeExceptionFrom(env, sdkError);
        GeJNIEnvCallVoidMethod(env, sdkThis, IntApnClient.pushDone, NULL, exception);
    } else {
        jstring id = (*env)->NewStringUTF(env, sdkId);
        GeJNIEnvCallVoidMethod(env, sdkThis, IntApnClient.pushDone, id, NULL);
    }

    if ((*GeJavaVM)->DetachCurrentThread(GeJavaVM) < JNI_OK) return;
}

JNIEXPORT jobject JNICALL Java_library_deintercom_IntApnClient_client(JNIEnv *env, jclass class, jstring base, jobject credential) {
    jobject this = NULL;
    INTAPNMulti *object = NULL;
    gchar *sdkBase = GeJNIEnvGetStringUTFChars(env, base, NULL);
    g_autoptr(APNCredential) sdkCredential = ApnCredentialTo(env, credential);
    g_autoptr(GError) sdkError = NULL;

    if ((object = int_apn_multi_new(sdkBase, sdkCredential, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        jobject listeners = (*env)->NewObject(env, IntApnClientListeners.class, IntApnClientListeners.init);

        this = (*env)->NewObject(env, class, IntApnClient.init);
        (*env)->SetLongField(env, this, IntApnClient.object, GPOINTER_TO_SIZE(object));
        (*env)->SetObjectField(env, this, IntApnClient.base, base);
        (*env)->SetObjectField(env, this, IntApnClient.credential, credential);
        (*env)->SetObjectField(env, this, IntApnClient.listeners, listeners);

        jweak weak = (*env)->NewWeakGlobalRef(env, this);
        (*env)->SetLongField(env, this, IntApnClient.weak, GPOINTER_TO_SIZE(weak));
        (void)g_signal_connect(object, "push-done", G_CALLBACK(IntApnClientINTAPNMultiPushDone), weak);
    }

    GeJNIEnvReleaseStringUTFChars(env, base, sdkBase);

    return this;
}

JNIEXPORT void JNICALL Java_library_deintercom_IntApnClient_abort(JNIEnv *env, jobject this) {

}

JNIEXPORT jlong JNICALL Java_library_deintercom_IntApnClient_pushCall(JNIEnv *env, jobject this, jobject pushCall) {
    jlong ret = 0;
    APNCRLEasy *sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntApnClient.object);
    g_autoptr(INTPushCall) sdkPushCall = IntPushCallTo(env, pushCall);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = int_apn_multi_push_call(GSIZE_TO_POINTER(object), sdkPushCall, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = GPOINTER_TO_SIZE(sdkRet);
    }

    return ret;
}

JNIEXPORT jlong JNICALL Java_library_deintercom_IntApnClient_pushIP(JNIEnv *env, jobject this, jobject pushIP) {
    jlong ret = 0;
    APNCRLEasy *sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntApnClient.object);
    g_autoptr(INTPushIP) sdkPushIP = IntPushIPTo(env, pushIP);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = int_apn_multi_push_ip(GSIZE_TO_POINTER(object), sdkPushIP, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = GPOINTER_TO_SIZE(sdkRet);
    }

    return ret;
}

JNIEXPORT jlong JNICALL Java_library_deintercom_IntApnClient_pushAccept(JNIEnv *env, jobject this, jobject pushAccept) {
    jlong ret = 0;
    APNCRLEasy *sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntApnClient.object);
    g_autoptr(INTPushAccept) sdkPushAccept = IntPushAcceptTo(env, pushAccept);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = int_apn_multi_push_accept(GSIZE_TO_POINTER(object), sdkPushAccept, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = GPOINTER_TO_SIZE(sdkRet);
    }

    return ret;
}










IntApnClientListenersType IntApnClientListeners = {0};

void IntApnClientListenersLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntApnClient$Listeners");
    IntApnClientListeners.class = (*env)->NewGlobalRef(env, class);
    IntApnClientListeners.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntApnClientListenersUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntApnClientListeners.class);
}
