//
// Created by Dan on 24.06.2022.
//

#ifndef LIBRARY_DEINTERCOM_INTAPNCLIENT_H
#define LIBRARY_DEINTERCOM_INTAPNCLIENT_H

#include "IntMain.h"
#include "IntSchema.h"










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID object;
    jfieldID weak;
    jfieldID base;
    jfieldID credential;
    jfieldID listeners;
    jmethodID init;
    jmethodID pushDone;
} IntApnClientType;

extern IntApnClientType IntApnClient;

void IntApnClientLoad(JNIEnv *env);
void IntApnClientUnload(JNIEnv *env);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jmethodID init;
} IntApnClientListenersType;

extern IntApnClientListenersType IntApnClientListeners;

void IntApnClientListenersLoad(JNIEnv *env);
void IntApnClientListenersUnload(JNIEnv *env);

G_END_DECLS










#endif //LIBRARY_DEINTERCOM_INTAPNCLIENT_H
