//
// Created by Dan on 05.10.2024.
//

#include "IntAsrModel.h"

IntAsrModelType IntAsrModel = {0};

void IntAsrModelLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntAsrModel");
    IntAsrModel.class = (*env)->NewGlobalRef(env, class);
    IntAsrModel.object = (*env)->GetFieldID(env, class, "object", "J");
    IntAsrModel.directory = (*env)->GetFieldID(env, class, "directory", "Ljava/lang/String;");
    IntAsrModel.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntAsrModelUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntAsrModel.class);
}

JNIEXPORT void JNICALL Java_library_deintercom_IntAsrModel_close(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, IntAsrModel.object);
    if (object == 0) return;
    g_object_unref(GSIZE_TO_POINTER(object));
    (*env)->SetLongField(env, this, IntAsrModel.object, 0);
}

JNIEXPORT jobject JNICALL Java_library_deintercom_IntAsrModel_modelSync(JNIEnv *env, jclass class, jstring directory) {
    jobject this = NULL;
    INTVSKModel *object = NULL;
    gchar *sdkDirectory = GeJNIEnvGetStringUTFChars(env, directory, NULL);
    g_autoptr(GError) sdkError = NULL;

    if ((object = int_vsk_model_new_sync(sdkDirectory, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        this = (*env)->NewObject(env, class, IntAsrModel.init);
        (*env)->SetLongField(env, this, IntAsrModel.object, GPOINTER_TO_SIZE(object));
        (*env)->SetObjectField(env, this, IntAsrModel.directory, directory);
    }

    GeJNIEnvReleaseStringUTFChars(env, directory, sdkDirectory);

    return this;
}
