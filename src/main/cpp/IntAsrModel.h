//
// Created by Dan on 05.10.2024.
//

#ifndef LIBRARY_DEINTERCOM_INTASRMODEL_H
#define LIBRARY_DEINTERCOM_INTASRMODEL_H

#include "IntMain.h"

G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID object;
    jfieldID directory;
    jmethodID init;
} IntAsrModelType;

extern IntAsrModelType IntAsrModel;

void IntAsrModelLoad(JNIEnv *env);
void IntAsrModelUnload(JNIEnv *env);

G_END_DECLS

#endif //LIBRARY_DEINTERCOM_INTASRMODEL_H
