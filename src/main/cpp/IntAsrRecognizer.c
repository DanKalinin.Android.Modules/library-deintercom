//
// Created by Dan on 05.10.2024.
//

#include "IntAsrRecognizer.h"

IntAsrRecognizerType IntAsrRecognizer = {0};

void IntAsrRecognizerLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntAsrRecognizer");
    IntAsrRecognizer.class = (*env)->NewGlobalRef(env, class);
    IntAsrRecognizer.object = (*env)->GetFieldID(env, class, "object", "J");
    IntAsrRecognizer.model = (*env)->GetFieldID(env, class, "model", "Llibrary/deintercom/IntAsrModel;");
    IntAsrRecognizer.sampleRate = (*env)->GetFieldID(env, class, "sampleRate", "F");
    IntAsrRecognizer.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntAsrRecognizerUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntAsrRecognizer.class);
}

JNIEXPORT void JNICALL Java_library_deintercom_IntAsrRecognizer_close(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, IntAsrRecognizer.object);
    if (object == 0) return;
    g_object_unref(GSIZE_TO_POINTER(object));
    (*env)->SetLongField(env, this, IntAsrRecognizer.object, 0);
}

JNIEXPORT jobject JNICALL Java_library_deintercom_IntAsrRecognizer_recognizer(JNIEnv *env, jclass class, jobject model, jfloat sampleRate) {
    jobject this = NULL;
    INTVSKRecognizer *object = NULL;
    jlong sdkModel = GeJNIEnvGetLongField(env, model, IntAsrModel.object);
    g_autoptr(GError) sdkError = NULL;

    if ((object = int_vsk_recognizer_new(GSIZE_TO_POINTER(sdkModel), sampleRate, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        this = (*env)->NewObject(env, class, IntAsrRecognizer.init);
        (*env)->SetLongField(env, this, IntAsrRecognizer.object, GPOINTER_TO_SIZE(object));
        (*env)->SetObjectField(env, this, IntAsrRecognizer.model, model);
        (*env)->SetFloatField(env, this, IntAsrRecognizer.sampleRate, sampleRate);
    }

    return this;
}

JNIEXPORT jobject JNICALL Java_library_deintercom_IntAsrRecognizer_recognizeSync(JNIEnv *env, jobject this, jshortArray data, jint n) {
    jobject ret = NULL;
    g_autoptr(VSKResult) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntAsrRecognizer.object);
    jshort *sdkData = (*env)->GetShortArrayElements(env, data, NULL);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = int_vsk_recognizer_recognize_sync(GSIZE_TO_POINTER(object), sdkData, n, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = VskResultFrom(env, sdkRet);
    }

    (*env)->ReleaseShortArrayElements(env, data, sdkData, JNI_ABORT);

    return ret;
}
