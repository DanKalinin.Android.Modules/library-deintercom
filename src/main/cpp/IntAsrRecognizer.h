//
// Created by Dan on 05.10.2024.
//

#ifndef LIBRARY_DEINTERCOM_INTASRRECOGNIZER_H
#define LIBRARY_DEINTERCOM_INTASRRECOGNIZER_H

#include "IntMain.h"
#include "IntAsrModel.h"

G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID object;
    jfieldID model;
    jfieldID sampleRate;
    jmethodID init;
} IntAsrRecognizerType;

extern IntAsrRecognizerType IntAsrRecognizer;

void IntAsrRecognizerLoad(JNIEnv *env);
void IntAsrRecognizerUnload(JNIEnv *env);

G_END_DECLS

#endif //LIBRARY_DEINTERCOM_INTASRRECOGNIZER_H
