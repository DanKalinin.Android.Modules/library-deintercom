//
// Created by Dan on 19.05.2022.
//

#include "IntClient.h"










IntClientType IntClient = {0};

void IntClientLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntClient");
    IntClient.class = (*env)->NewGlobalRef(env, class);
    IntClient.object = (*env)->GetFieldID(env, class, "object", "J");
    IntClient.weak = (*env)->GetFieldID(env, class, "weak", "J");
    IntClient.base = (*env)->GetFieldID(env, class, "base", "Ljava/lang/String;");
    IntClient.username = (*env)->GetFieldID(env, class, "username", "Ljava/lang/String;");
    IntClient.password = (*env)->GetFieldID(env, class, "password", "Ljava/lang/String;");
    IntClient.database = (*env)->GetFieldID(env, class, "database", "Llibrary/deintercom/IntDatabase;");
    IntClient.listeners = (*env)->GetFieldID(env, class, "listeners", "Llibrary/deintercom/IntClient$Listeners;");
    IntClient.init = (*env)->GetMethodID(env, class, "<init>", "()V");
    IntClient.call = (*env)->GetMethodID(env, class, "call", "(B)V");
    IntClient.ip = (*env)->GetMethodID(env, class, "ip", "(Ljava/lang/String;Ljava/lang/String;)V");
    IntClient.accept = (*env)->GetMethodID(env, class, "accept", "(Ljava/lang/String;B)V");
    IntClient.progress = (*env)->GetMethodID(env, class, "progress", "(JJI)V");
}

void IntClientUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntClient.class);
}

JNIEXPORT void JNICALL Java_library_deintercom_IntClient_close(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    if (object == 0) return;
    g_object_unref(GSIZE_TO_POINTER(object));
    (*env)->SetLongField(env, this, IntClient.object, 0);

    jlong weak = (*env)->GetLongField(env, this, IntClient.weak);
    (*env)->DeleteWeakGlobalRef(env, GSIZE_TO_POINTER(weak));
}

void IntClientINTSOESessionCall(INTSOESession *sdkSender, guint8 sdkA, gpointer sdkThis) {
    JNIEnv *env = NULL;
    if ((*GeJavaVM)->GetEnv(GeJavaVM, (void **)&env, JNI_VERSION_1_2) < JNI_OK) return;
    GeJNIEnvCallVoidMethod(env, sdkThis, IntClient.call, sdkA);
}

void IntClientINTSOESessionIP(INTSOESession *sdkSender, gchar *sdkLocal, gchar *sdkExternal, gpointer sdkThis) {
    JNIEnv *env = NULL;
    if ((*GeJavaVM)->GetEnv(GeJavaVM, (void **)&env, JNI_VERSION_1_2) < JNI_OK) return;
    jstring local = (*env)->NewStringUTF(env, sdkLocal);
    jstring external = (*env)->NewStringUTF(env, sdkExternal);
    GeJNIEnvCallVoidMethod(env, sdkThis, IntClient.ip, local, external);
}

void IntClientINTSOESessionAccept(INTSOESession *sdkSender, gchar *sdkId, guint8 sdkA, gpointer sdkThis) {
    JNIEnv *env = NULL;
    if ((*GeJavaVM)->GetEnv(GeJavaVM, (void **)&env, JNI_VERSION_1_2) < JNI_OK) return;
    jstring id = (*env)->NewStringUTF(env, sdkId);
    GeJNIEnvCallVoidMethod(env, sdkThis, IntClient.accept, id, sdkA);
}

void IntClientINTSOESessionProgress(INTSOESession *sdkSender, goffset sdkCurrent, goffset sdkTotal, gint sdkTag, gpointer sdkThis) {
    JNIEnv *env = NULL;
    if ((*GeJavaVM)->GetEnv(GeJavaVM, (void **)&env, JNI_VERSION_1_2) < JNI_OK) return;
    GeJNIEnvCallVoidMethod(env, sdkThis, IntClient.progress, sdkCurrent, sdkTotal, sdkTag);
}

JNIEXPORT jobject JNICALL Java_library_deintercom_IntClient_client(JNIEnv *env, jclass class, jstring base, jstring username, jstring password, jobject database) {
    gchar *sdkBase = GeJNIEnvGetStringUTFChars(env, base, NULL);
    g_autoptr(SoupURI) sdkBase1 = soup_uri_new(sdkBase);
    gchar *sdkUsername = GeJNIEnvGetStringUTFChars(env, username, NULL);
    gchar *sdkPassword = GeJNIEnvGetStringUTFChars(env, password, NULL);
    jlong sdkConnection = GeJNIEnvGetLongField(env, database, IntDatabase.object);
    INTSOESession *object = int_soe_session_new(sdkBase1, sdkUsername, sdkPassword, GSIZE_TO_POINTER(sdkConnection));

    jobject listeners = (*env)->NewObject(env, IntClientListeners.class, IntClientListeners.init);

    jobject this = (*env)->NewObject(env, class, IntClient.init);
    (*env)->SetLongField(env, this, IntClient.object, GPOINTER_TO_SIZE(object));
    (*env)->SetObjectField(env, this, IntClient.base, base);
    (*env)->SetObjectField(env, this, IntClient.username, username);
    (*env)->SetObjectField(env, this, IntClient.password, password);
    (*env)->SetObjectField(env, this, IntClient.database, database);
    (*env)->SetObjectField(env, this, IntClient.listeners, listeners);

    jweak weak = (*env)->NewWeakGlobalRef(env, this);
    (*env)->SetLongField(env, this, IntClient.weak, GPOINTER_TO_SIZE(weak));
    (void)g_signal_connect(object, "call", G_CALLBACK(IntClientINTSOESessionCall), weak);
    (void)g_signal_connect(object, "ip", G_CALLBACK(IntClientINTSOESessionIP), weak);
    (void)g_signal_connect(object, "accept", G_CALLBACK(IntClientINTSOESessionAccept), weak);
    (void)g_signal_connect(object, "progress", G_CALLBACK(IntClientINTSOESessionProgress), weak);

    GeJNIEnvReleaseStringUTFChars(env, base, sdkBase);
    GeJNIEnvReleaseStringUTFChars(env, username, sdkUsername);
    GeJNIEnvReleaseStringUTFChars(env, password, sdkPassword);

    return this;
}

JNIEXPORT void JNICALL Java_library_deintercom_IntClient_abort(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    soup_session_abort(GSIZE_TO_POINTER(object));
}

JNIEXPORT void JNICALL Java_library_deintercom_IntClient_cancel(JNIEnv *env, jobject this, jint tag) {
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    int_soe_session_cancel(GSIZE_TO_POINTER(object), tag);
}

JNIEXPORT void JNICALL Java_library_deintercom_IntClient_online(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    int_soe_session_online(GSIZE_TO_POINTER(object));
}

JNIEXPORT void JNICALL Java_library_deintercom_IntClient_offline(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    int_soe_session_offline(GSIZE_TO_POINTER(object));
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntClient_answer(JNIEnv *env, jobject this, jbyte a) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_soe_session_answer(GSIZE_TO_POINTER(object), a, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntClient_open(JNIEnv *env, jobject this, jbyte a) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_soe_session_open(GSIZE_TO_POINTER(object), a, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntClient_readSync(JNIEnv *env, jobject this) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_soe_session_read_sync(GSIZE_TO_POINTER(object), &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntClient_pingSync(JNIEnv *env, jobject this) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_soe_session_ping_sync(GSIZE_TO_POINTER(object), &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jobject JNICALL Java_library_deintercom_IntClient_registerSync(JNIEnv *env, jobject this, jobject frontend) {
    jobject ret = NULL;
    g_autoptr(INTBackend) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    g_autoptr(INTFrontend) sdkFrontend = IntFrontendTo(env, frontend);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = int_soe_session_register_sync(GSIZE_TO_POINTER(object), sdkFrontend, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = IntBackendFrom(env, sdkRet);
    }

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntClient_unregister(JNIEnv *env, jobject this) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_soe_session_unregister(GSIZE_TO_POINTER(object), &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jobject JNICALL Java_library_deintercom_IntClient_qrCodeSync(JNIEnv *env, jobject this) {
    jobject ret = NULL;
    g_autoptr(INTQRCode) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = int_soe_session_qr_code_sync(GSIZE_TO_POINTER(object), &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = IntQrCodeFrom(env, sdkRet);
    }

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_deintercom_IntClient_callsSync(JNIEnv *env, jobject this, jlong started) {
    jobjectArray ret = NULL;
    g_autolist(INTCall) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_soe_session_calls_sync(GSIZE_TO_POINTER(object), started, &sdkRet, &sdkError)) {
        ret = IntCallsFrom(env, sdkRet);
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntClient_uploadSync(JNIEnv *env, jobject this, jstring name, jstring file, jint tag) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    gchar *sdkName = GeJNIEnvGetStringUTFChars(env, name, NULL);
    gchar *sdkFile = GeJNIEnvGetStringUTFChars(env, file, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (int_soe_session_upload_sync(GSIZE_TO_POINTER(object), sdkName, sdkFile, tag, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, name, sdkName);
    GeJNIEnvReleaseStringUTFChars(env, file, sdkFile);

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntClient_downloadSync(JNIEnv *env, jobject this, jstring name, jstring file, jint tag) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    gchar *sdkName = GeJNIEnvGetStringUTFChars(env, name, NULL);
    gchar *sdkFile = GeJNIEnvGetStringUTFChars(env, file, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (int_soe_session_download_sync(GSIZE_TO_POINTER(object), sdkName, sdkFile, tag, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, name, sdkName);
    GeJNIEnvReleaseStringUTFChars(env, file, sdkFile);

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_deintercom_IntClient_filesSync(JNIEnv *env, jobject this, jstring directory, jlong modified) {
    jobjectArray ret = NULL;
    g_autolist(INTFile) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    gchar *sdkDirectory = GeJNIEnvGetStringUTFChars(env, directory, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (int_soe_session_files_sync(GSIZE_TO_POINTER(object), sdkDirectory, modified, &sdkRet, &sdkError)) {
        ret = IntFilesFrom(env, sdkRet);
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, directory, sdkDirectory);

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_deintercom_IntClient_passwords(JNIEnv *env, jobject this) {
    jobjectArray ret = NULL;
    g_autolist(INTPassword) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_soe_session_passwords(GSIZE_TO_POINTER(object), &sdkRet, &sdkError)) {
        ret = IntPasswordsFrom(env, sdkRet);
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jobject JNICALL Java_library_deintercom_IntClient_passwordUpload(JNIEnv *env, jobject this, jstring name, jstring file, jint tag) {
    jobject ret = NULL;
    g_autoptr(INTPassword) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    gchar *sdkName = GeJNIEnvGetStringUTFChars(env, name, NULL);
    gchar *sdkFile = GeJNIEnvGetStringUTFChars(env, file, NULL);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = int_soe_session_password_upload(GSIZE_TO_POINTER(object), sdkName, sdkFile, tag, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = IntPasswordFrom(env, sdkRet);
    }

    GeJNIEnvReleaseStringUTFChars(env, name, sdkName);
    GeJNIEnvReleaseStringUTFChars(env, file, sdkFile);

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntClient_passwordEnable(JNIEnv *env, jobject this, jstring name, jboolean enabled) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    gchar *sdkName = GeJNIEnvGetStringUTFChars(env, name, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (int_soe_session_password_enable(GSIZE_TO_POINTER(object), sdkName, enabled, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, name, sdkName);

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntClient_passwordDelete(JNIEnv *env, jobject this, jstring name) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    gchar *sdkName = GeJNIEnvGetStringUTFChars(env, name, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (int_soe_session_password_delete(GSIZE_TO_POINTER(object), sdkName, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, name, sdkName);

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_deintercom_IntClient_frontends(JNIEnv *env, jobject this) {
    jobjectArray ret = NULL;
    g_autolist(INTFrontend) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_soe_session_frontends(GSIZE_TO_POINTER(object), &sdkRet, &sdkError)) {
        ret = IntFrontendsFrom(env, sdkRet);
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntClient_frontendDelete(JNIEnv *env, jobject this, jstring id) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    gchar *sdkId = GeJNIEnvGetStringUTFChars(env, id, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (int_soe_session_frontend_delete(GSIZE_TO_POINTER(object), sdkId, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, id, sdkId);

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_deintercom_IntClient_callsV1Sync(JNIEnv *env, jobject this) {
    jobjectArray ret = NULL;
    g_autolist(INTCall) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_soe_session_calls_v1_sync(GSIZE_TO_POINTER(object), &sdkRet, &sdkError)) {
        ret = IntCallsFrom(env, sdkRet);
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_deintercom_IntClient_filesV1Sync(JNIEnv *env, jobject this, jstring directory) {
    jobjectArray ret = NULL;
    g_autolist(INTFile) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntClient.object);
    gchar *sdkDirectory = GeJNIEnvGetStringUTFChars(env, directory, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (int_soe_session_files_v1_sync(GSIZE_TO_POINTER(object), sdkDirectory, &sdkRet, &sdkError)) {
        ret = IntFilesFrom(env, sdkRet);
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, directory, sdkDirectory);

    return ret;
}










IntClientListenersType IntClientListeners = {0};

void IntClientListenersLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntClient$Listeners");
    IntClientListeners.class = (*env)->NewGlobalRef(env, class);
    IntClientListeners.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntClientListenersUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntClientListeners.class);
}
