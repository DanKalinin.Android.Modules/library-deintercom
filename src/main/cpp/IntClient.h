//
// Created by Dan on 19.05.2022.
//

#ifndef LIBRARY_DEINTERCOM_INTCLIENT_H
#define LIBRARY_DEINTERCOM_INTCLIENT_H

#include "IntMain.h"
#include "IntDatabase.h"










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID object;
    jfieldID weak;
    jfieldID base;
    jfieldID username;
    jfieldID password;
    jfieldID database;
    jfieldID listeners;
    jmethodID init;
    jmethodID call;
    jmethodID ip;
    jmethodID accept;
    jmethodID progress;
} IntClientType;

extern IntClientType IntClient;

void IntClientLoad(JNIEnv *env);
void IntClientUnload(JNIEnv *env);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jmethodID init;
} IntClientListenersType;

extern IntClientListenersType IntClientListeners;

void IntClientListenersLoad(JNIEnv *env);
void IntClientListenersUnload(JNIEnv *env);

G_END_DECLS










#endif //LIBRARY_DEINTERCOM_INTCLIENT_H
