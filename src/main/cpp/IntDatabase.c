//
// Created by Dan on 15.05.2022.
//

#include "IntDatabase.h"










IntDatabaseType IntDatabase = {0};

void IntDatabaseLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntDatabase");
    IntDatabase.class = (*env)->NewGlobalRef(env, class);
    IntDatabase.object = (*env)->GetFieldID(env, class, "object", "J");
    IntDatabase.weak = (*env)->GetFieldID(env, class, "weak", "J");
    IntDatabase.file = (*env)->GetFieldID(env, class, "file", "Ljava/lang/String;");
    IntDatabase.listeners = (*env)->GetFieldID(env, class, "listeners", "Llibrary/deintercom/IntDatabase$Listeners;");
    IntDatabase.init = (*env)->GetMethodID(env, class, "<init>", "()V");
    IntDatabase.commit = (*env)->GetMethodID(env, class, "commit", "()V");
}

void IntDatabaseUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntDatabase.class);
}

JNIEXPORT void JNICALL Java_library_deintercom_IntDatabase_close(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, IntDatabase.object);
    if (object == 0) return;
    g_object_unref(GSIZE_TO_POINTER(object));
    (*env)->SetLongField(env, this, IntDatabase.object, 0);

    jlong weak = (*env)->GetLongField(env, this, IntDatabase.weak);
    (*env)->DeleteWeakGlobalRef(env, GSIZE_TO_POINTER(weak));
}

gint IntDatabaseSQLEConnectionCommit(SQLEConnection *sdkSender, gpointer sdkThis) {
    JNIEnv *env = NULL;
    if ((*GeJavaVM)->GetEnv(GeJavaVM, (void **)&env, JNI_VERSION_1_2) < JNI_OK) return SQLITE_OK;
    GeJNIEnvCallVoidMethod(env, sdkThis, IntDatabase.commit);
    return SQLITE_OK;
}

JNIEXPORT jobject JNICALL Java_library_deintercom_IntDatabase_database(JNIEnv *env, jclass class, jstring file) {
    jobject this = NULL;
    INTSQLEConnection *object = NULL;
    gchar *sdkFile = GeJNIEnvGetStringUTFChars(env, file, NULL);
    g_autoptr(GError) sdkError = NULL;

    if ((object = int_sqle_connection_new(sdkFile, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        jobject listeners = (*env)->NewObject(env, IntDatabaseListeners.class, IntDatabaseListeners.init);

        this = (*env)->NewObject(env, class, IntDatabase.init);
        (*env)->SetLongField(env, this, IntDatabase.object, GPOINTER_TO_SIZE(object));
        (*env)->SetObjectField(env, this, IntDatabase.file, file);
        (*env)->SetObjectField(env, this, IntDatabase.listeners, listeners);

        jweak weak = (*env)->NewWeakGlobalRef(env, this);
        (*env)->SetLongField(env, this, IntDatabase.weak, GPOINTER_TO_SIZE(weak));
        (void)g_signal_connect(object, "commit", G_CALLBACK(IntDatabaseSQLEConnectionCommit), weak);
    }

    GeJNIEnvReleaseStringUTFChars(env, file, sdkFile);

    return this;
}

JNIEXPORT jintArray JNICALL Java_library_deintercom_IntDatabase_selectInt(JNIEnv *env, jobject this, jstring tail) {
    jintArray ret = NULL;
    g_autoptr(GList) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntDatabase.object);
    INTSQLEConnection *sdkObject = GSIZE_TO_POINTER(object);
    gchar *sdkTail = GeJNIEnvGetStringUTFChars(env, tail, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (sqle_connection_select_int(SQLE_CONNECTION(sdkObject)->object, sdkTail, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = GeJNIEnvJIntArrayGListGIntFrom(env, sdkRet);
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, tail, sdkTail);

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_deintercom_IntDatabase_selectText(JNIEnv *env, jobject this, jstring tail) {
    jobjectArray ret = NULL;
    g_autolist(gchar) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntDatabase.object);
    INTSQLEConnection *sdkObject = GSIZE_TO_POINTER(object);
    gchar *sdkTail = GeJNIEnvGetStringUTFChars(env, tail, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (sqle_connection_select_text(SQLE_CONNECTION(sdkObject)->object, sdkTail, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = GeJNIEnvJObjectArrayGListGCharArrayFrom(env, sdkRet);
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, tail, sdkTail);

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntDatabase_backendUpsert(JNIEnv *env, jobject this, jobject backend) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntDatabase.object);
    g_autoptr(INTBackend) sdkBackend = IntBackendTo(env, backend);
    g_autoptr(GError) sdkError = NULL;

    if (int_sqle_connection_backend_upsert(GSIZE_TO_POINTER(object), sdkBackend, &sdkError) == SQLITE_DONE) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntDatabase_backendDeleteById(JNIEnv *env, jobject this, jstring id) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntDatabase.object);
    gchar *sdkId = GeJNIEnvGetStringUTFChars(env, id, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (int_sqle_connection_backend_delete_by_id(GSIZE_TO_POINTER(object), sdkId, &sdkError) == SQLITE_DONE) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, id, sdkId);

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_deintercom_IntDatabase_backendSelect(JNIEnv *env, jobject this, jstring tail) {
    jobjectArray ret = NULL;
    g_autolist(INTBackend) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntDatabase.object);
    gchar *sdkTail = GeJNIEnvGetStringUTFChars(env, tail, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (int_sqle_connection_backend_select(GSIZE_TO_POINTER(object), sdkTail, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = IntBackendsFrom(env, sdkRet);
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, tail, sdkTail);

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_deintercom_IntDatabase_backendSelectById(JNIEnv *env, jobject this, jstring id) {
    jobjectArray ret = NULL;
    g_autolist(INTBackend) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntDatabase.object);
    gchar *sdkId = GeJNIEnvGetStringUTFChars(env, id, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (int_sqle_connection_backend_select_by_id(GSIZE_TO_POINTER(object), sdkId, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = IntBackendsFrom(env, sdkRet);
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, id, sdkId);

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_deintercom_IntDatabase_callSelectByBackend(JNIEnv *env, jobject this, jstring backend, jstring tail) {
    jobjectArray ret = NULL;
    g_autolist(INTCall) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntDatabase.object);
    gchar *sdkBackend = GeJNIEnvGetStringUTFChars(env, backend, NULL);
    gchar *sdkTail = GeJNIEnvGetStringUTFChars(env, tail, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (int_sqle_connection_call_select_by_backend(GSIZE_TO_POINTER(object), sdkBackend, sdkTail, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = IntCallsFrom(env, sdkRet);
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, backend, sdkBackend);
    GeJNIEnvReleaseStringUTFChars(env, tail, sdkTail);

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_deintercom_IntDatabase_fileSelectByBackendDirectory(JNIEnv *env, jobject this, jstring backend, jstring directory, jstring tail) {
    jobjectArray ret = NULL;
    g_autolist(INTFile) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntDatabase.object);
    gchar *sdkBackend = GeJNIEnvGetStringUTFChars(env, backend, NULL);
    gchar *sdkDirectory = GeJNIEnvGetStringUTFChars(env, directory, NULL);
    gchar *sdkTail = GeJNIEnvGetStringUTFChars(env, tail, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (int_sqle_connection_file_select_by_backend_directory(GSIZE_TO_POINTER(object), sdkBackend, sdkDirectory, sdkTail, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = IntFilesFrom(env, sdkRet);
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, backend, sdkBackend);
    GeJNIEnvReleaseStringUTFChars(env, directory, sdkDirectory);
    GeJNIEnvReleaseStringUTFChars(env, tail, sdkTail);

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_deintercom_IntDatabase_fileSelectByBackendDirectoryModifiedBetween(JNIEnv *env, jobject this, jstring backend, jstring directory, jlong modified1, jlong modified2, jstring tail) {
    jobjectArray ret = NULL;
    g_autolist(INTFile) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntDatabase.object);
    gchar *sdkBackend = GeJNIEnvGetStringUTFChars(env, backend, NULL);
    gchar *sdkDirectory = GeJNIEnvGetStringUTFChars(env, directory, NULL);
    gchar *sdkTail = GeJNIEnvGetStringUTFChars(env, tail, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (int_sqle_connection_file_select_by_backend_directory_modified_between(GSIZE_TO_POINTER(object), sdkBackend, sdkDirectory, modified1, modified2, sdkTail, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = IntFilesFrom(env, sdkRet);
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, backend, sdkBackend);
    GeJNIEnvReleaseStringUTFChars(env, directory, sdkDirectory);
    GeJNIEnvReleaseStringUTFChars(env, tail, sdkTail);

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_deintercom_IntDatabase_passwordSelectByBackendName(JNIEnv *env, jobject this, jstring backend, jstring name) {
    jobjectArray ret = NULL;
    g_autolist(INTPassword) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, IntDatabase.object);
    gchar *sdkBackend = GeJNIEnvGetStringUTFChars(env, backend, NULL);
    gchar *sdkName = GeJNIEnvGetStringUTFChars(env, name, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (int_sqle_connection_password_select_by_backend_name(GSIZE_TO_POINTER(object), sdkBackend, sdkName, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = IntPasswordsFrom(env, sdkRet);
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, backend, sdkBackend);
    GeJNIEnvReleaseStringUTFChars(env, name, sdkName);

    return ret;
}










IntDatabaseListenersType IntDatabaseListeners = {0};

void IntDatabaseListenersLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntDatabase$Listeners");
    IntDatabaseListeners.class = (*env)->NewGlobalRef(env, class);
    IntDatabaseListeners.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntDatabaseListenersUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntDatabaseListeners.class);
}
