//
// Created by Dan on 15.05.2022.
//

#ifndef LIBRARY_DEINTERCOM_INTDATABASE_H
#define LIBRARY_DEINTERCOM_INTDATABASE_H

#include "IntMain.h"
#include "IntSchema.h"










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID object;
    jfieldID weak;
    jfieldID file;
    jfieldID listeners;
    jmethodID init;
    jmethodID commit;
} IntDatabaseType;

extern IntDatabaseType IntDatabase;

void IntDatabaseLoad(JNIEnv *env);
void IntDatabaseUnload(JNIEnv *env);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jmethodID init;
} IntDatabaseListenersType;

extern IntDatabaseListenersType IntDatabaseListeners;

void IntDatabaseListenersLoad(JNIEnv *env);
void IntDatabaseListenersUnload(JNIEnv *env);

G_END_DECLS










#endif //LIBRARY_DEINTERCOM_INTDATABASE_H
