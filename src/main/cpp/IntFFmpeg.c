//
// Created by Dan on 26.10.2024.
//

#include "IntFFmpeg.h"

JNIEXPORT jdouble JNICALL Java_library_deintercom_IntFFmpeg_duration(JNIEnv *env, jclass class, jstring file) {
    jdouble ret = -1.0;
    gdouble sdkRet = -1.0;
    gchar *sdkFile = GeJNIEnvGetStringUTFChars(env, file, NULL);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = int_ffmpeg_duration(sdkFile, &sdkError)) == -1.0) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = sdkRet;
    }

    GeJNIEnvReleaseStringUTFChars(env, file, sdkFile);

    return ret;
}

JNIEXPORT jstring JNICALL Java_library_deintercom_IntFFmpeg_lyrics(JNIEnv *env, jclass class, jstring file) {
    jstring ret = NULL;
    g_autofree gchar *sdkRet = NULL;
    gchar *sdkFile = GeJNIEnvGetStringUTFChars(env, file, NULL);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = int_ffmpeg_lyrics(sdkFile, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = (*env)->NewStringUTF(env, sdkRet);
    }

    GeJNIEnvReleaseStringUTFChars(env, file, sdkFile);

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntFFmpeg_convertPcm(JNIEnv *env, jclass class, jstring input, jstring output, jint rate, jdouble from, jdouble to, jstring lyrics) {
    jboolean ret = JNI_FALSE;
    gchar *sdkInput = GeJNIEnvGetStringUTFChars(env, input, NULL);
    gchar *sdkOutput = GeJNIEnvGetStringUTFChars(env, output, NULL);
    gchar *sdkLyrics = GeJNIEnvGetStringUTFChars(env, lyrics, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (int_ffmpeg_convert_pcm(sdkInput, sdkOutput, rate, from, to, sdkLyrics, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, input, sdkInput);
    GeJNIEnvReleaseStringUTFChars(env, output, sdkOutput);
    GeJNIEnvReleaseStringUTFChars(env, lyrics, sdkLyrics);

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntFFmpeg_convertPcmData(JNIEnv *env, jclass class, jshortArray input, jint n, jstring output, jint rate, jdouble from, jdouble to, jstring lyrics) {
    jboolean ret = JNI_FALSE;
    jshort *sdkInput = (*env)->GetShortArrayElements(env, input, NULL);
    gchar *sdkOutput = GeJNIEnvGetStringUTFChars(env, output, NULL);
    gchar *sdkLyrics = GeJNIEnvGetStringUTFChars(env, lyrics, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (int_ffmpeg_convert_pcm_data(sdkInput, n, sdkOutput, rate, from, to, sdkLyrics, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    (*env)->ReleaseShortArrayElements(env, input, sdkInput, JNI_ABORT);
    GeJNIEnvReleaseStringUTFChars(env, output, sdkOutput);
    GeJNIEnvReleaseStringUTFChars(env, lyrics, sdkLyrics);

    return ret;
}
