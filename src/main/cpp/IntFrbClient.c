//
// Created by Dan on 25.06.2022.
//

#include "IntFrbClient.h"










IntFrbClientType IntFrbClient = {0};

void IntFrbClientLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntFrbClient");
    IntFrbClient.class = (*env)->NewGlobalRef(env, class);
    IntFrbClient.object = (*env)->GetFieldID(env, class, "object", "J");
    IntFrbClient.weak = (*env)->GetFieldID(env, class, "weak", "J");
    IntFrbClient.base = (*env)->GetFieldID(env, class, "base", "Ljava/lang/String;");
    IntFrbClient.credential = (*env)->GetFieldID(env, class, "credential", "Llibrary/firebaseext/FrbCredential;");
    IntFrbClient.listeners = (*env)->GetFieldID(env, class, "listeners", "Llibrary/deintercom/IntFrbClient$Listeners;");
    IntFrbClient.init = (*env)->GetMethodID(env, class, "<init>", "()V");
    IntFrbClient.pushDone = (*env)->GetMethodID(env, class, "pushDone", "(Ljava/lang/String;Llibrary/glibext/GeException;)V");
}

void IntFrbClientUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntFrbClient.class);
}

JNIEXPORT void JNICALL Java_library_deintercom_IntFrbClient_close(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, IntFrbClient.object);
    if (object == 0) return;
    g_object_unref(GSIZE_TO_POINTER(object));
    (*env)->SetLongField(env, this, IntFrbClient.object, 0);

    jlong weak = (*env)->GetLongField(env, this, IntFrbClient.weak);
    (*env)->DeleteWeakGlobalRef(env, GSIZE_TO_POINTER(weak));
}

void IntFrbClientINTFRBSessionPushDone(INTFRBSession *sdkSender, gchar *sdkName, GError *sdkError, gpointer sdkThis) {
    JNIEnv *env = NULL;
    if ((*GeJavaVM)->AttachCurrentThread(GeJavaVM, &env, NULL) < JNI_OK) return;

    if (sdkName == NULL) {
        jthrowable exception = GeExceptionFrom(env, sdkError);
        GeJNIEnvCallVoidMethod(env, sdkThis, IntFrbClient.pushDone, NULL, exception);
    } else {
        jstring name = (*env)->NewStringUTF(env, sdkName);
        GeJNIEnvCallVoidMethod(env, sdkThis, IntFrbClient.pushDone, name, NULL);
    }

    if ((*GeJavaVM)->DetachCurrentThread(GeJavaVM) < JNI_OK) return;
}

JNIEXPORT jobject JNICALL Java_library_deintercom_IntFrbClient_client(JNIEnv *env, jclass class, jstring base, jobject credential) {
    gchar *sdkBase = GeJNIEnvGetStringUTFChars(env, base, NULL);
    g_autoptr(SoupURI) sdkBase1 = soup_uri_new(sdkBase);
    g_autoptr(FRBCredential) sdkCredential = FrbCredentialTo(env, credential);
    INTFRBSession *object = int_frb_session_new(sdkBase1, sdkCredential);

    jobject listeners = (*env)->NewObject(env, IntFrbClientListeners.class, IntFrbClientListeners.init);

    jobject this = (*env)->NewObject(env, class, IntFrbClient.init);
    (*env)->SetLongField(env, this, IntFrbClient.object, GPOINTER_TO_SIZE(object));
    (*env)->SetObjectField(env, this, IntFrbClient.base, base);
    (*env)->SetObjectField(env, this, IntFrbClient.credential, credential);
    (*env)->SetObjectField(env, this, IntFrbClient.listeners, listeners);

    jweak weak = (*env)->NewWeakGlobalRef(env, this);
    (*env)->SetLongField(env, this, IntFrbClient.weak, GPOINTER_TO_SIZE(weak));
    (void)g_signal_connect(object, "push-done", G_CALLBACK(IntFrbClientINTFRBSessionPushDone), weak);

    GeJNIEnvReleaseStringUTFChars(env, base, sdkBase);

    return this;
}

JNIEXPORT void JNICALL Java_library_deintercom_IntFrbClient_abort(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, IntFrbClient.object);
    soup_session_abort(GSIZE_TO_POINTER(object));
}

JNIEXPORT void JNICALL Java_library_deintercom_IntFrbClient_pushCall(JNIEnv *env, jobject this, jobject pushCall) {
    jlong object = (*env)->GetLongField(env, this, IntFrbClient.object);
    g_autoptr(INTPushCall) sdkPushCall = IntPushCallTo(env, pushCall);
    int_frb_session_push_call(GSIZE_TO_POINTER(object), sdkPushCall);
}

JNIEXPORT void JNICALL Java_library_deintercom_IntFrbClient_pushIP(JNIEnv *env, jobject this, jobject pushIP) {
    jlong object = (*env)->GetLongField(env, this, IntFrbClient.object);
    g_autoptr(INTPushIP) sdkPushIP = IntPushIPTo(env, pushIP);
    int_frb_session_push_ip(GSIZE_TO_POINTER(object), sdkPushIP);
}

JNIEXPORT void JNICALL Java_library_deintercom_IntFrbClient_pushAccept(JNIEnv *env, jobject this, jobject pushAccept) {
    jlong object = (*env)->GetLongField(env, this, IntFrbClient.object);
    g_autoptr(INTPushAccept) sdkPushAccept = IntPushAcceptTo(env, pushAccept);
    int_frb_session_push_accept(GSIZE_TO_POINTER(object), sdkPushAccept);
}










IntFrbClientListenersType IntFrbClientListeners = {0};

void IntFrbClientListenersLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntFrbClient$Listeners");
    IntFrbClientListeners.class = (*env)->NewGlobalRef(env, class);
    IntFrbClientListeners.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntFrbClientListenersUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntFrbClientListeners.class);
}
