//
// Created by Dan on 25.06.2022.
//

#ifndef LIBRARY_DEINTERCOM_INTFRBCLIENT_H
#define LIBRARY_DEINTERCOM_INTFRBCLIENT_H

#include "IntMain.h"
#include "IntSchema.h"










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID object;
    jfieldID weak;
    jfieldID base;
    jfieldID credential;
    jfieldID listeners;
    jmethodID init;
    jmethodID pushDone;
} IntFrbClientType;

extern IntFrbClientType IntFrbClient;

void IntFrbClientLoad(JNIEnv *env);
void IntFrbClientUnload(JNIEnv *env);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jmethodID init;
} IntFrbClientListenersType;

extern IntFrbClientListenersType IntFrbClientListeners;

void IntFrbClientListenersLoad(JNIEnv *env);
void IntFrbClientListenersUnload(JNIEnv *env);

G_END_DECLS










#endif //LIBRARY_DEINTERCOM_INTFRBCLIENT_H
