//
// Created by Dan on 28.07.2022.
//

#include "IntIgd.h"










IntIgdType IntIgd = {0};

void IntIgdLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntIgd");
    IntIgd.class = (*env)->NewGlobalRef(env, class);
    IntIgd.object = (*env)->GetFieldID(env, class, "object", "J");
    IntIgd.weak = (*env)->GetFieldID(env, class, "weak", "J");
    IntIgd.http = (*env)->GetFieldID(env, class, "http", "I");
    IntIgd.sip = (*env)->GetFieldID(env, class, "sip", "I");
    IntIgd.local = (*env)->GetFieldID(env, class, "local", "Ljava/lang/String;");
    IntIgd.external = (*env)->GetFieldID(env, class, "external", "Ljava/lang/String;");
    IntIgd.listeners = (*env)->GetFieldID(env, class, "listeners", "Llibrary/deintercom/IntIgd$Listeners;");
    IntIgd.init = (*env)->GetMethodID(env, class, "<init>", "()V");
    IntIgd.ip = (*env)->GetMethodID(env, class, "ip", "(Ljava/lang/String;Ljava/lang/String;)V");
    IntIgd.error = (*env)->GetMethodID(env, class, "error", "(Llibrary/glibext/GeException;)V");
}

void IntIgdUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntIgd.class);
}

JNIEXPORT void JNICALL Java_library_deintercom_IntIgd_close(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, IntIgd.object);
    if (object == 0) return;
    g_object_unref(GSIZE_TO_POINTER(object));
    (*env)->SetLongField(env, this, IntIgd.object, 0);

    jlong weak = (*env)->GetLongField(env, this, IntIgd.weak);
    (*env)->DeleteWeakGlobalRef(env, GSIZE_TO_POINTER(weak));
}

void IntIgdINTIGDIP(INTIGD *sdkSender, gchar *sdkLocal, gchar *sdkExternal, gpointer sdkThis) {
    JNIEnv *env = NULL;
    if ((*GeJavaVM)->AttachCurrentThread(GeJavaVM, &env, NULL) < JNI_OK) return;

    jstring local = (*env)->NewStringUTF(env, sdkLocal);
    jstring external = (*env)->NewStringUTF(env, sdkExternal);
    (*env)->SetObjectField(env, sdkThis, IntIgd.local, local);
    (*env)->SetObjectField(env, sdkThis, IntIgd.external, external);
    GeJNIEnvCallVoidMethod(env, sdkThis, IntIgd.ip, local, external);

    if ((*GeJavaVM)->DetachCurrentThread(GeJavaVM) < JNI_OK) return;
}

void IntIgdINTIGDError(INTIGD *sdkSender, GError *sdkError, gpointer sdkThis) {
    JNIEnv *env = NULL;
    if ((*GeJavaVM)->AttachCurrentThread(GeJavaVM, &env, NULL) < JNI_OK) return;

    jthrowable exception = GeExceptionFrom(env, sdkError);
    GeJNIEnvCallVoidMethod(env, sdkThis, IntIgd.error, exception);

    if ((*GeJavaVM)->DetachCurrentThread(GeJavaVM) < JNI_OK) return;
}

JNIEXPORT jobject JNICALL Java_library_deintercom_IntIgd_igd(JNIEnv *env, jclass class, jstring iface, jint http, jint sip) {
    gchar *sdkIface = GeJNIEnvGetStringUTFChars(env, iface, NULL);
    INTIGD *object = int_igd_new(sdkIface, http, sip);

    jobject listeners = (*env)->NewObject(env, IntIgdListeners.class, IntIgdListeners.init);

    jobject this = (*env)->NewObject(env, class, IntIgd.init);
    (*env)->SetLongField(env, this, IntIgd.object, GPOINTER_TO_SIZE(object));
    (*env)->SetIntField(env, this, IntIgd.http, http);
    (*env)->SetIntField(env, this, IntIgd.sip, sip);
    (*env)->SetObjectField(env, this, IntIgd.listeners, listeners);

    jweak weak = (*env)->NewWeakGlobalRef(env, this);
    (*env)->SetLongField(env, this, IntIgd.weak, GPOINTER_TO_SIZE(weak));
    (void)g_signal_connect(object, "ip", G_CALLBACK(IntIgdINTIGDIP), weak);
    (void)g_signal_connect(object, "error", G_CALLBACK(IntIgdINTIGDError), weak);

    GeJNIEnvReleaseStringUTFChars(env, iface, sdkIface);

    return this;
}










IntIgdListenersType IntIgdListeners = {0};

void IntIgdListenersLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntIgd$Listeners");
    IntIgdListeners.class = (*env)->NewGlobalRef(env, class);
    IntIgdListeners.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntIgdListenersUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntIgdListeners.class);
}
