//
// Created by Dan on 28.07.2022.
//

#ifndef LIBRARY_DEINTERCOM_INTIGD_H
#define LIBRARY_DEINTERCOM_INTIGD_H

#include "IntMain.h"










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID object;
    jfieldID weak;
    jfieldID http;
    jfieldID sip;
    jfieldID local;
    jfieldID external;
    jfieldID listeners;
    jmethodID init;
    jmethodID ip;
    jmethodID error;
} IntIgdType;

extern IntIgdType IntIgd;

void IntIgdLoad(JNIEnv *env);
void IntIgdUnload(JNIEnv *env);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jmethodID init;
} IntIgdListenersType;

extern IntIgdListenersType IntIgdListeners;

void IntIgdListenersLoad(JNIEnv *env);
void IntIgdListenersUnload(JNIEnv *env);

G_END_DECLS










#endif //LIBRARY_DEINTERCOM_INTIGD_H
