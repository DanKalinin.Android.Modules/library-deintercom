//
// Created by Dan on 05.12.2021.
//

#include "IntInit.h"

jint JNI_OnLoad(JavaVM *vm, gpointer reserved) {
    int_init();

    JNIEnv *env = NULL;
    (void)(*vm)->GetEnv(vm, (gpointer *)&env, JNI_VERSION_1_2);

    IntPushCallLoad(env);
    IntPushIPLoad(env);
    IntPushAcceptLoad(env);
    IntBackendLoad(env);
    IntFrontendLoad(env);
    IntQrCodeLoad(env);
    IntTextLoad(env);
    IntCallLoad(env);
    IntFileLoad(env);
    IntMessageLoad(env);
    IntToneLoad(env);
    IntPasswordLoad(env);
    IntPtyFsLoad(env);
    IntSerialLoad(env);
    IntSerialListenersLoad(env);
    IntUsbSerialLoad(env);
    IntUsbSerialListenersLoad(env);
    IntDatabaseLoad(env);
    IntDatabaseListenersLoad(env);
    IntServerLoad(env);
    IntClientLoad(env);
    IntClientListenersLoad(env);
    IntApnClientLoad(env);
    IntApnClientListenersLoad(env);
    IntFrbClientLoad(env);
    IntFrbClientListenersLoad(env);
    IntIgdLoad(env);
    IntIgdListenersLoad(env);
    IntPingerLoad(env);
    IntPingerListenersLoad(env);
    IntSipAgentLoad(env);
    IntProgrammerLoad(env);
    IntProgrammerListenersLoad(env);
    IntAsrModelLoad(env);
    IntAsrRecognizerLoad(env);

    return JNI_VERSION_1_2;
}

void JNI_OnUnload(JavaVM *vm, gpointer reserved) {
    JNIEnv *env = NULL;
    (void)(*vm)->GetEnv(vm, (gpointer *)&env, JNI_VERSION_1_2);

    IntPushCallUnload(env);
    IntPushIPUnload(env);
    IntPushAcceptUnload(env);
    IntBackendUnload(env);
    IntFrontendUnload(env);
    IntQrCodeUnload(env);
    IntTextUnload(env);
    IntCallUnload(env);
    IntFileUnload(env);
    IntMessageUnload(env);
    IntToneUnload(env);
    IntPasswordUnload(env);
    IntPtyFsUnload(env);
    IntSerialUnload(env);
    IntSerialListenersUnload(env);
    IntUsbSerialUnload(env);
    IntUsbSerialListenersUnload(env);
    IntDatabaseUnload(env);
    IntDatabaseListenersUnload(env);
    IntServerUnload(env);
    IntClientUnload(env);
    IntClientListenersUnload(env);
    IntApnClientUnload(env);
    IntApnClientListenersUnload(env);
    IntFrbClientUnload(env);
    IntFrbClientListenersUnload(env);
    IntIgdUnload(env);
    IntIgdListenersUnload(env);
    IntPingerUnload(env);
    IntPingerListenersUnload(env);
    IntSipAgentUnload(env);
    IntProgrammerUnload(env);
    IntProgrammerListenersUnload(env);
    IntAsrModelUnload(env);
    IntAsrRecognizerUnload(env);
}
