//
// Created by Dan on 08.09.2022.
//

#include "IntJwt.h"

JNIEXPORT jstring JNICALL Java_library_deintercom_IntJwt_qrCodeEncode(JNIEnv *env, jclass class, jobject qrCode, jstring key) {
    jstring ret = NULL;
    g_autofree gchar *sdkRet = NULL;
    g_autoptr(INTQRCode) sdkQrCode = IntQrCodeTo(env, qrCode);
    gchar *sdkKey = GeJNIEnvGetStringUTFChars(env, key, NULL);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = int_jwt_qr_code_encode(sdkQrCode, sdkKey, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = (*env)->NewStringUTF(env, sdkRet);
    }

    GeJNIEnvReleaseStringUTFChars(env, key, sdkKey);

    return ret;
}

JNIEXPORT jobject JNICALL Java_library_deintercom_IntJwt_qrCodeDecode(JNIEnv *env, jclass class, jstring token, jstring key) {
    jobject ret = NULL;
    g_autoptr(INTQRCode) sdkRet = NULL;
    gchar *sdkToken = GeJNIEnvGetStringUTFChars(env, token, NULL);
    gchar *sdkKey = GeJNIEnvGetStringUTFChars(env, key, NULL);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = int_jwt_qr_code_decode(sdkToken, sdkKey, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = IntQrCodeFrom(env, sdkRet);
    }

    GeJNIEnvReleaseStringUTFChars(env, token, sdkToken);
    GeJNIEnvReleaseStringUTFChars(env, key, sdkKey);

    return ret;
}
