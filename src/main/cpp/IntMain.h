//
// Created by Dan on 05.12.2021.
//

#ifndef LIBRARY_DEINTERCOM_INTMAIN_H
#define LIBRARY_DEINTERCOM_INTMAIN_H

#include <de-intercom/de-intercom.h>
#include <GlibExt.h>
#include <SoupExt.h>
#include <GnuTLSExt.h>
#include <GLibNetworkingExt.h>
#include <JsonExt.h>
#include <XMLExt.h>
#include <AvahiExt.h>
#include <SQLiteExt.h>
#include <PjsipExt.h>
#include <PtyExt.h>
#include <USBExt.h>
#include <FirebaseExt.h>
#include <CurlExt.h>
#include <APNExt.h>
#include <JWT2Ext.h>
#include <GSSDPExt.h>
#include <GUPnPExt.h>
#include <GUPnPIGDExt.h>
#include <NiceExt.h>
#include <AsteriskExt.h>
#include <FFmpegExt.h>
#include <FDroidExt.h>
#include <AVRDudeExt.h>
#include <VoskExt.h>

#endif //LIBRARY_DEINTERCOM_INTMAIN_H
