//
// Created by Dan on 09.08.2022.
//

#include "IntPinger.h"










IntPingerType IntPinger = {0};

void IntPingerLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntPinger");
    IntPinger.class = (*env)->NewGlobalRef(env, class);
    IntPinger.object = (*env)->GetFieldID(env, class, "object", "J");
    IntPinger.weak = (*env)->GetFieldID(env, class, "weak", "J");
    IntPinger.local = (*env)->GetFieldID(env, class, "local", "Llibrary/deintercom/IntClient;");
    IntPinger.external = (*env)->GetFieldID(env, class, "external", "Llibrary/deintercom/IntClient;");
    IntPinger.current = (*env)->GetFieldID(env, class, "current", "Llibrary/deintercom/IntClient;");
    IntPinger.listeners = (*env)->GetFieldID(env, class, "listeners", "Llibrary/deintercom/IntPinger$Listeners;");
    IntPinger.init = (*env)->GetMethodID(env, class, "<init>", "()V");
    IntPinger.pong = (*env)->GetMethodID(env, class, "pong", "(Llibrary/deintercom/IntClient;)V");
}

void IntPingerUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntPinger.class);
}

JNIEXPORT void JNICALL Java_library_deintercom_IntPinger_close(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, IntPinger.object);
    if (object == 0) return;
    g_object_unref(GSIZE_TO_POINTER(object));
    (*env)->SetLongField(env, this, IntPinger.object, 0);

    jlong weak = (*env)->GetLongField(env, this, IntPinger.weak);
    (*env)->DeleteWeakGlobalRef(env, GSIZE_TO_POINTER(weak));
}

void IntPingerINTPingerPong(INTPinger *sdkSender, INTSOESession *sdkCurrent, gpointer sdkThis) {
    JNIEnv *env = NULL;
    if ((*GeJavaVM)->AttachCurrentThread(GeJavaVM, &env, NULL) < JNI_OK) return;

    jobject local = (*env)->GetObjectField(env, sdkThis, IntPinger.local);
    jobject external = (*env)->GetObjectField(env, sdkThis, IntPinger.external);
    jlong sdkLocal = (*env)->GetLongField(env, local, IntClient.object);

    if (sdkCurrent == GSIZE_TO_POINTER(sdkLocal)) {
        (*env)->SetObjectField(env, sdkThis, IntPinger.current, local);
        GeJNIEnvCallVoidMethod(env, sdkThis, IntPinger.pong, local);
    } else {
        (*env)->SetObjectField(env, sdkThis, IntPinger.current, external);
        GeJNIEnvCallVoidMethod(env, sdkThis, IntPinger.pong, external);
    }

    if ((*GeJavaVM)->DetachCurrentThread(GeJavaVM) < JNI_OK) return;
}

JNIEXPORT jobject JNICALL Java_library_deintercom_IntPinger_pinger(JNIEnv *env, jclass class, jobject local, jobject external, jobject current) {
    jlong sdkLocal = GeJNIEnvGetLongField(env, local, IntClient.object);
    jlong sdkExternal = GeJNIEnvGetLongField(env, external, IntClient.object);
    jlong sdkCurrent = GeJNIEnvGetLongField(env, current, IntClient.object);
    INTPinger *object = int_pinger_new(GSIZE_TO_POINTER(sdkLocal), GSIZE_TO_POINTER(sdkExternal), GSIZE_TO_POINTER(sdkCurrent));

    jobject listeners = (*env)->NewObject(env, IntPingerListeners.class, IntPingerListeners.init);

    jobject this = (*env)->NewObject(env, class, IntPinger.init);
    (*env)->SetLongField(env, this, IntPinger.object, GPOINTER_TO_SIZE(object));
    (*env)->SetObjectField(env, this, IntPinger.local, local);
    (*env)->SetObjectField(env, this, IntPinger.external, external);
    (*env)->SetObjectField(env, this, IntPinger.current, current);
    (*env)->SetObjectField(env, this, IntPinger.listeners, listeners);

    jweak weak = (*env)->NewWeakGlobalRef(env, this);
    (*env)->SetLongField(env, this, IntPinger.weak, GPOINTER_TO_SIZE(weak));
    (void)g_signal_connect(object, "pong", G_CALLBACK(IntPingerINTPingerPong), weak);

    return this;
}

JNIEXPORT void JNICALL Java_library_deintercom_IntPinger_ping(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, IntPinger.object);
    int_pinger_ping(GSIZE_TO_POINTER(object));
}










IntPingerListenersType IntPingerListeners = {0};

void IntPingerListenersLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntPinger$Listeners");
    IntPingerListeners.class = (*env)->NewGlobalRef(env, class);
    IntPingerListeners.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntPingerListenersUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntPingerListeners.class);
}
