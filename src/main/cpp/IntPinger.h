//
// Created by Dan on 09.08.2022.
//

#ifndef LIBRARY_DEINTERCOM_INTPINGER_H
#define LIBRARY_DEINTERCOM_INTPINGER_H

#include "IntMain.h"
#include "IntClient.h"










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID object;
    jfieldID weak;
    jfieldID local;
    jfieldID external;
    jfieldID current;
    jfieldID listeners;
    jmethodID init;
    jmethodID pong;
} IntPingerType;

extern IntPingerType IntPinger;

void IntPingerLoad(JNIEnv *env);
void IntPingerUnload(JNIEnv *env);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jmethodID init;
} IntPingerListenersType;

extern IntPingerListenersType IntPingerListeners;

void IntPingerListenersLoad(JNIEnv *env);
void IntPingerListenersUnload(JNIEnv *env);

G_END_DECLS










#endif //LIBRARY_DEINTERCOM_INTPINGER_H
