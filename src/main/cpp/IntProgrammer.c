//
// Created by Dan on 01.05.2024.
//

#include "IntProgrammer.h"










IntProgrammerType IntProgrammer = {0};

void IntProgrammerLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntProgrammer");
    IntProgrammer.class = (*env)->NewGlobalRef(env, class);
    IntProgrammer.object = (*env)->GetFieldID(env, class, "object", "J");
    IntProgrammer.weak = (*env)->GetFieldID(env, class, "weak", "J");
    IntProgrammer.listeners = (*env)->GetFieldID(env, class, "listeners", "Llibrary/deintercom/IntProgrammer$Listeners;");
    IntProgrammer.init = (*env)->GetMethodID(env, class, "<init>", "()V");
    IntProgrammer.progress = (*env)->GetMethodID(env, class, "progress", "(IDLjava/lang/String;I)V");
}

void IntProgrammerUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntProgrammer.class);
}

JNIEXPORT void JNICALL Java_library_deintercom_IntProgrammer_close(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, IntProgrammer.object);
    if (object == 0) return;
    g_object_unref(GSIZE_TO_POINTER(object));
    (*env)->SetLongField(env, this, IntProgrammer.object, 0);

    jlong weak = (*env)->GetLongField(env, this, IntProgrammer.weak);
    (*env)->DeleteWeakGlobalRef(env, GSIZE_TO_POINTER(weak));
}

void IntProgrammerAVDProgrammerProgress(AVDProgrammer *sdkSender, gint sdkPercent, gdouble sdkTime, gchar *sdkHeader, gint sdkFinish, gpointer sdkThis) {
    JNIEnv *env = NULL;
    if ((*GeJavaVM)->GetEnv(GeJavaVM, (void **)&env, JNI_VERSION_1_2) < JNI_OK) return;
    jstring header = (*env)->NewStringUTF(env, sdkHeader);
    GeJNIEnvCallVoidMethod(env, sdkThis, IntProgrammer.progress, sdkPercent, sdkTime, header, sdkFinish);
}

JNIEXPORT jobject JNICALL Java_library_deintercom_IntProgrammer_programmer(JNIEnv *env, jclass class) {
    INTAVDProgrammer *object = int_avd_programmer_new();

    jobject listeners = (*env)->NewObject(env, IntProgrammerListeners.class, IntProgrammerListeners.init);

    jobject this = (*env)->NewObject(env, class, IntProgrammer.init);
    (*env)->SetLongField(env, this, IntProgrammer.object, GPOINTER_TO_SIZE(object));
    (*env)->SetObjectField(env, this, IntProgrammer.listeners, listeners);

    jweak weak = (*env)->NewWeakGlobalRef(env, this);
    (*env)->SetLongField(env, this, IntProgrammer.weak, GPOINTER_TO_SIZE(weak));
    (void)g_signal_connect(object, "progress", G_CALLBACK(IntProgrammerAVDProgrammerProgress), weak);

    return this;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntProgrammer_updateSync(JNIEnv *env, jobject this, jstring config, jstring firmware, jstring port) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntProgrammer.object);
    gchar *sdkConfig = GeJNIEnvGetStringUTFChars(env, config, NULL);
    gchar *sdkFirmware = GeJNIEnvGetStringUTFChars(env, firmware, NULL);
    gchar *sdkPort = GeJNIEnvGetStringUTFChars(env, port, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (int_avd_programmer_update_sync(GSIZE_TO_POINTER(object), sdkConfig, sdkFirmware, sdkPort, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, config, sdkConfig);
    GeJNIEnvReleaseStringUTFChars(env, firmware, sdkFirmware);
    GeJNIEnvReleaseStringUTFChars(env, port, sdkPort);

    return ret;
}










IntProgrammerListenersType IntProgrammerListeners = {0};

void IntProgrammerListenersLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntProgrammer$Listeners");
    IntProgrammerListeners.class = (*env)->NewGlobalRef(env, class);
    IntProgrammerListeners.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntProgrammerListenersUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntProgrammerListeners.class);
}
