//
// Created by Dan on 01.05.2024.
//

#ifndef LIBRARY_DEINTERCOM_INTPROGRAMMER_H
#define LIBRARY_DEINTERCOM_INTPROGRAMMER_H

#include "IntMain.h"










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID object;
    jfieldID weak;
    jfieldID listeners;
    jmethodID init;
    jmethodID progress;
} IntProgrammerType;

extern IntProgrammerType IntProgrammer;

void IntProgrammerLoad(JNIEnv *env);
void IntProgrammerUnload(JNIEnv *env);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jmethodID init;
} IntProgrammerListenersType;

extern IntProgrammerListenersType IntProgrammerListeners;

void IntProgrammerListenersLoad(JNIEnv *env);
void IntProgrammerListenersUnload(JNIEnv *env);

G_END_DECLS










#endif //LIBRARY_DEINTERCOM_INTPROGRAMMER_H
