//
// Created by Dan on 12.12.2021.
//

#include "IntPtyFs.h"

IntPtyFsType IntPtyFs = {0};

void IntPtyFsLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntPtyFs");
    IntPtyFs.class = (*env)->NewGlobalRef(env, class);
    IntPtyFs.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntPtyFsUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntPtyFs.class);
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntPtyFs_mountSync(JNIEnv *env, jclass class, jstring directory) {
    jboolean ret = JNI_FALSE;
    gchar *sdkDirectory = GeJNIEnvGetStringUTFChars(env, directory, NULL);
    g_autoptr(GError) sdkError = NULL;

    if (int_pty_fs_mount_sync(sdkDirectory, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, directory, sdkDirectory);

    return ret;
}
