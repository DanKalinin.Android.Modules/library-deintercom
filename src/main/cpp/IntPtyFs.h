//
// Created by Dan on 12.12.2021.
//

#ifndef LIBRARY_DEINTERCOM_INTPTYFS_H
#define LIBRARY_DEINTERCOM_INTPTYFS_H

#include "IntMain.h"

G_BEGIN_DECLS

typedef struct {
    jclass class;
    jmethodID init;
} IntPtyFsType;

extern IntPtyFsType IntPtyFs;

void IntPtyFsLoad(JNIEnv *env);
void IntPtyFsUnload(JNIEnv *env);

G_END_DECLS

#endif //LIBRARY_DEINTERCOM_INTPTYFS_H
