//
// Created by Dan on 23.06.2022.
//

#include "IntSchema.h"










IntPushCallType IntPushCall = {0};

void IntPushCallLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntPushCall");
    IntPushCall.class = (*env)->NewGlobalRef(env, class);
    IntPushCall.project = (*env)->GetFieldID(env, class, "project", "Ljava/lang/String;");
    IntPushCall.token = (*env)->GetFieldID(env, class, "token", "Ljava/lang/String;");
    IntPushCall.id = (*env)->GetFieldID(env, class, "id", "Ljava/lang/String;");
    IntPushCall.a = (*env)->GetFieldID(env, class, "a", "B");
    IntPushCall.local = (*env)->GetFieldID(env, class, "local", "Ljava/lang/String;");
    IntPushCall.external = (*env)->GetFieldID(env, class, "external", "Ljava/lang/String;");
    IntPushCall.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntPushCallUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntPushCall.class);
}

jobject IntPushCallFrom(JNIEnv *env, INTPushCall *sdkPushCall) {
    jstring project = (*env)->NewStringUTF(env, sdkPushCall->project);
    jstring token = (*env)->NewStringUTF(env, sdkPushCall->token);
    jstring id = (*env)->NewStringUTF(env, sdkPushCall->id);
    jstring local = (*env)->NewStringUTF(env, sdkPushCall->local);
    jstring external = (*env)->NewStringUTF(env, sdkPushCall->external);

    jobject this = (*env)->NewObject(env, IntPushCall.class, IntPushCall.init);
    (*env)->SetObjectField(env, this, IntPushCall.project, project);
    (*env)->SetObjectField(env, this, IntPushCall.token, token);
    (*env)->SetObjectField(env, this, IntPushCall.id, id);
    (*env)->SetByteField(env, this, IntPushCall.a, sdkPushCall->a);
    (*env)->SetObjectField(env, this, IntPushCall.local, local);
    (*env)->SetObjectField(env, this, IntPushCall.external, external);
    return this;
}

INTPushCall *IntPushCallTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring project = (*env)->GetObjectField(env, this, IntPushCall.project);
    jstring token = (*env)->GetObjectField(env, this, IntPushCall.token);
    jstring id = (*env)->GetObjectField(env, this, IntPushCall.id);
    jstring local = (*env)->GetObjectField(env, this, IntPushCall.local);
    jstring external = (*env)->GetObjectField(env, this, IntPushCall.external);

    INTPushCall sdkPushCall = {0};
    sdkPushCall.project = GeJNIEnvGetStringUTFChars(env, project, NULL);
    sdkPushCall.token = GeJNIEnvGetStringUTFChars(env, token, NULL);
    sdkPushCall.id = GeJNIEnvGetStringUTFChars(env, id, NULL);
    sdkPushCall.a = (*env)->GetByteField(env, this, IntPushCall.a);
    sdkPushCall.local = GeJNIEnvGetStringUTFChars(env, local, NULL);
    sdkPushCall.external = GeJNIEnvGetStringUTFChars(env, external, NULL);
    INTPushCall *ret = int_push_call_copy(&sdkPushCall);

    GeJNIEnvReleaseStringUTFChars(env, project, sdkPushCall.project);
    GeJNIEnvReleaseStringUTFChars(env, token, sdkPushCall.token);
    GeJNIEnvReleaseStringUTFChars(env, id, sdkPushCall.id);
    GeJNIEnvReleaseStringUTFChars(env, local, sdkPushCall.local);
    GeJNIEnvReleaseStringUTFChars(env, external, sdkPushCall.external);

    return ret;
}










IntPushIPType IntPushIP = {0};

void IntPushIPLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntPushIP");
    IntPushIP.class = (*env)->NewGlobalRef(env, class);
    IntPushIP.project = (*env)->GetFieldID(env, class, "project", "Ljava/lang/String;");
    IntPushIP.token = (*env)->GetFieldID(env, class, "token", "Ljava/lang/String;");
    IntPushIP.id = (*env)->GetFieldID(env, class, "id", "Ljava/lang/String;");
    IntPushIP.local = (*env)->GetFieldID(env, class, "local", "Ljava/lang/String;");
    IntPushIP.external = (*env)->GetFieldID(env, class, "external", "Ljava/lang/String;");
    IntPushIP.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntPushIPUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntPushIP.class);
}

jobject IntPushIPFrom(JNIEnv *env, INTPushIP *sdkPushIP) {
    jstring project = (*env)->NewStringUTF(env, sdkPushIP->project);
    jstring token = (*env)->NewStringUTF(env, sdkPushIP->token);
    jstring id = (*env)->NewStringUTF(env, sdkPushIP->id);
    jstring local = (*env)->NewStringUTF(env, sdkPushIP->local);
    jstring external = (*env)->NewStringUTF(env, sdkPushIP->external);

    jobject this = (*env)->NewObject(env, IntPushIP.class, IntPushIP.init);
    (*env)->SetObjectField(env, this, IntPushIP.project, project);
    (*env)->SetObjectField(env, this, IntPushIP.token, token);
    (*env)->SetObjectField(env, this, IntPushIP.id, id);
    (*env)->SetObjectField(env, this, IntPushIP.local, local);
    (*env)->SetObjectField(env, this, IntPushIP.external, external);
    return this;
}

INTPushIP *IntPushIPTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring project = (*env)->GetObjectField(env, this, IntPushIP.project);
    jstring token = (*env)->GetObjectField(env, this, IntPushIP.token);
    jstring id = (*env)->GetObjectField(env, this, IntPushIP.id);
    jstring local = (*env)->GetObjectField(env, this, IntPushIP.local);
    jstring external = (*env)->GetObjectField(env, this, IntPushIP.external);

    INTPushIP sdkPushIP = {0};
    sdkPushIP.project = GeJNIEnvGetStringUTFChars(env, project, NULL);
    sdkPushIP.token = GeJNIEnvGetStringUTFChars(env, token, NULL);
    sdkPushIP.id = GeJNIEnvGetStringUTFChars(env, id, NULL);
    sdkPushIP.local = GeJNIEnvGetStringUTFChars(env, local, NULL);
    sdkPushIP.external = GeJNIEnvGetStringUTFChars(env, external, NULL);
    INTPushIP *ret = int_push_ip_copy(&sdkPushIP);

    GeJNIEnvReleaseStringUTFChars(env, project, sdkPushIP.project);
    GeJNIEnvReleaseStringUTFChars(env, token, sdkPushIP.token);
    GeJNIEnvReleaseStringUTFChars(env, id, sdkPushIP.id);
    GeJNIEnvReleaseStringUTFChars(env, local, sdkPushIP.local);
    GeJNIEnvReleaseStringUTFChars(env, external, sdkPushIP.external);

    return ret;
}










IntPushAcceptType IntPushAccept = {0};

void IntPushAcceptLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntPushAccept");
    IntPushAccept.class = (*env)->NewGlobalRef(env, class);
    IntPushAccept.project = (*env)->GetFieldID(env, class, "project", "Ljava/lang/String;");
    IntPushAccept.token = (*env)->GetFieldID(env, class, "token", "Ljava/lang/String;");
    IntPushAccept.backendId = (*env)->GetFieldID(env, class, "backendId", "Ljava/lang/String;");
    IntPushAccept.frontendId = (*env)->GetFieldID(env, class, "frontendId", "Ljava/lang/String;");
    IntPushAccept.a = (*env)->GetFieldID(env, class, "a", "B");
    IntPushAccept.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntPushAcceptUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntPushAccept.class);
}

jobject IntPushAcceptFrom(JNIEnv *env, INTPushAccept *sdkPushAccept) {
    jstring project = (*env)->NewStringUTF(env, sdkPushAccept->project);
    jstring token = (*env)->NewStringUTF(env, sdkPushAccept->token);
    jstring backendId = (*env)->NewStringUTF(env, sdkPushAccept->backend_id);
    jstring frontendId = (*env)->NewStringUTF(env, sdkPushAccept->frontend_id);

    jobject this = (*env)->NewObject(env, IntPushAccept.class, IntPushAccept.init);
    (*env)->SetObjectField(env, this, IntPushAccept.project, project);
    (*env)->SetObjectField(env, this, IntPushAccept.token, token);
    (*env)->SetObjectField(env, this, IntPushAccept.backendId, backendId);
    (*env)->SetObjectField(env, this, IntPushAccept.frontendId, frontendId);
    (*env)->SetByteField(env, this, IntPushAccept.a, sdkPushAccept->a);
    return this;
}

INTPushAccept *IntPushAcceptTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring project = (*env)->GetObjectField(env, this, IntPushAccept.project);
    jstring token = (*env)->GetObjectField(env, this, IntPushAccept.token);
    jstring backendId = (*env)->GetObjectField(env, this, IntPushAccept.backendId);
    jstring frontendId = (*env)->GetObjectField(env, this, IntPushAccept.frontendId);

    INTPushAccept sdkPushAccept = {0};
    sdkPushAccept.project = GeJNIEnvGetStringUTFChars(env, project, NULL);
    sdkPushAccept.token = GeJNIEnvGetStringUTFChars(env, token, NULL);
    sdkPushAccept.backend_id = GeJNIEnvGetStringUTFChars(env, backendId, NULL);
    sdkPushAccept.frontend_id = GeJNIEnvGetStringUTFChars(env, frontendId, NULL);
    sdkPushAccept.a = (*env)->GetByteField(env, this, IntPushAccept.a);
    INTPushAccept *ret = int_push_accept_copy(&sdkPushAccept);

    GeJNIEnvReleaseStringUTFChars(env, project, sdkPushAccept.project);
    GeJNIEnvReleaseStringUTFChars(env, token, sdkPushAccept.token);
    GeJNIEnvReleaseStringUTFChars(env, backendId, sdkPushAccept.backend_id);
    GeJNIEnvReleaseStringUTFChars(env, frontendId, sdkPushAccept.frontend_id);

    return ret;
}










IntBackendType IntBackend = {0};

void IntBackendLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntBackend");
    IntBackend.class = (*env)->NewGlobalRef(env, class);
    IntBackend.id = (*env)->GetFieldID(env, class, "id", "Ljava/lang/String;");
    IntBackend.name = (*env)->GetFieldID(env, class, "name", "Ljava/lang/String;");
    IntBackend.local = (*env)->GetFieldID(env, class, "local", "Ljava/lang/String;");
    IntBackend.external = (*env)->GetFieldID(env, class, "external", "Ljava/lang/String;");
    IntBackend.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntBackendUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntBackend.class);
}

jobject IntBackendFrom(JNIEnv *env, INTBackend *sdkBackend) {
    jstring id = (*env)->NewStringUTF(env, sdkBackend->id);
    jstring name = (*env)->NewStringUTF(env, sdkBackend->name);
    jstring local = (*env)->NewStringUTF(env, sdkBackend->local);
    jstring external = (*env)->NewStringUTF(env, sdkBackend->external);

    jobject this = (*env)->NewObject(env, IntBackend.class, IntBackend.init);
    (*env)->SetObjectField(env, this, IntBackend.id, id);
    (*env)->SetObjectField(env, this, IntBackend.name, name);
    (*env)->SetObjectField(env, this, IntBackend.local, local);
    (*env)->SetObjectField(env, this, IntBackend.external, external);
    return this;
}

INTBackend *IntBackendTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring id = (*env)->GetObjectField(env, this, IntBackend.id);
    jstring name = (*env)->GetObjectField(env, this, IntBackend.name);
    jstring local = (*env)->GetObjectField(env, this, IntBackend.local);
    jstring external = (*env)->GetObjectField(env, this, IntBackend.external);

    INTBackend sdkBackend = {0};
    sdkBackend.id = GeJNIEnvGetStringUTFChars(env, id, NULL);
    sdkBackend.name = GeJNIEnvGetStringUTFChars(env, name, NULL);
    sdkBackend.local = GeJNIEnvGetStringUTFChars(env, local, NULL);
    sdkBackend.external = GeJNIEnvGetStringUTFChars(env, external, NULL);
    INTBackend *ret = int_backend_copy(&sdkBackend);

    GeJNIEnvReleaseStringUTFChars(env, id, sdkBackend.id);
    GeJNIEnvReleaseStringUTFChars(env, name, sdkBackend.name);
    GeJNIEnvReleaseStringUTFChars(env, local, sdkBackend.local);
    GeJNIEnvReleaseStringUTFChars(env, external, sdkBackend.external);

    return ret;
}

jobjectArray IntBackendsFrom(JNIEnv *env, GList *sdkBackends) {
    guint n = g_list_length(sdkBackends);
    jobjectArray this = (*env)->NewObjectArray(env, n, IntBackend.class, NULL);
    jsize i = 0;

    for (GList *sdkBackend = sdkBackends; sdkBackend != NULL; sdkBackend = sdkBackend->next) {
        jobject backend = IntBackendFrom(env, sdkBackend->data);
        (*env)->SetObjectArrayElement(env, this, i, backend);
        i++;
    }

    return this;
}

GList *IntBackendsTo(JNIEnv *env, jobjectArray this) {
    GList *ret = NULL;
    jsize n = (*env)->GetArrayLength(env, this);

    for (jsize i = 0; i < n; i++) {
        jobject backend = (*env)->GetObjectArrayElement(env, this, i);
        INTBackend *sdkBackend = IntBackendTo(env, backend);
        ret = g_list_append(ret, sdkBackend);
    }

    return ret;
}










IntFrontendType IntFrontend = {0};

void IntFrontendLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntFrontend");
    IntFrontend.class = (*env)->NewGlobalRef(env, class);
    IntFrontend.backend = (*env)->GetFieldID(env, class, "backend", "Ljava/lang/String;");
    IntFrontend.id = (*env)->GetFieldID(env, class, "id", "Ljava/lang/String;");
    IntFrontend.os = (*env)->GetFieldID(env, class, "os", "I");
    IntFrontend.project = (*env)->GetFieldID(env, class, "project", "Ljava/lang/String;");
    IntFrontend.token = (*env)->GetFieldID(env, class, "token", "Ljava/lang/String;");
    IntFrontend.online = (*env)->GetFieldID(env, class, "online", "Z");
    IntFrontend.lastSeen = (*env)->GetFieldID(env, class, "lastSeen", "J");
    IntFrontend.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntFrontendUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntFrontend.class);
}

jobject IntFrontendFrom(JNIEnv *env, INTFrontend *sdkFrontend) {
    jstring backend = (*env)->NewStringUTF(env, sdkFrontend->backend);
    jstring id = (*env)->NewStringUTF(env, sdkFrontend->id);
    jstring project = (*env)->NewStringUTF(env, sdkFrontend->project);
    jstring token = (*env)->NewStringUTF(env, sdkFrontend->token);

    jobject this = (*env)->NewObject(env, IntFrontend.class, IntFrontend.init);
    (*env)->SetObjectField(env, this, IntFrontend.backend, backend);
    (*env)->SetObjectField(env, this, IntFrontend.id, id);
    (*env)->SetIntField(env, this, IntFrontend.os, sdkFrontend->os);
    (*env)->SetObjectField(env, this, IntFrontend.project, project);
    (*env)->SetObjectField(env, this, IntFrontend.token, token);
    (*env)->SetBooleanField(env, this, IntFrontend.online, sdkFrontend->online);
    (*env)->SetLongField(env, this, IntFrontend.lastSeen, sdkFrontend->last_seen);
    return this;
}

INTFrontend *IntFrontendTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring backend = (*env)->GetObjectField(env, this, IntFrontend.backend);
    jstring id = (*env)->GetObjectField(env, this, IntFrontend.id);
    jstring project = (*env)->GetObjectField(env, this, IntFrontend.project);
    jstring token = (*env)->GetObjectField(env, this, IntFrontend.token);

    INTFrontend sdkFrontend = {0};
    sdkFrontend.backend = GeJNIEnvGetStringUTFChars(env, backend, NULL);
    sdkFrontend.id = GeJNIEnvGetStringUTFChars(env, id, NULL);
    sdkFrontend.os = (*env)->GetIntField(env, this, IntFrontend.os);
    sdkFrontend.project = GeJNIEnvGetStringUTFChars(env, project, NULL);
    sdkFrontend.token = GeJNIEnvGetStringUTFChars(env, token, NULL);
    sdkFrontend.online = (*env)->GetBooleanField(env, this, IntFrontend.online);
    sdkFrontend.last_seen = (*env)->GetLongField(env, this, IntFrontend.lastSeen);
    INTFrontend *ret = int_frontend_copy(&sdkFrontend);

    GeJNIEnvReleaseStringUTFChars(env, backend, sdkFrontend.backend);
    GeJNIEnvReleaseStringUTFChars(env, id, sdkFrontend.id);
    GeJNIEnvReleaseStringUTFChars(env, project, sdkFrontend.project);
    GeJNIEnvReleaseStringUTFChars(env, token, sdkFrontend.token);

    return ret;
}

jobject IntFrontendsFrom(JNIEnv *env, GList *sdkFrontends) {
    guint n = g_list_length(sdkFrontends);
    jobjectArray this = (*env)->NewObjectArray(env, n, IntFrontend.class, NULL);
    jsize i = 0;

    for (GList *sdkFrontend = sdkFrontends; sdkFrontend != NULL; sdkFrontend = sdkFrontend->next) {
        jobject frontend = IntFrontendFrom(env, sdkFrontend->data);
        (*env)->SetObjectArrayElement(env, this, i, frontend);
        i++;
    }

    return this;
}

GList *IntFrontendsTo(JNIEnv *env, jobjectArray this) {
    GList *ret = NULL;
    jsize n = (*env)->GetArrayLength(env, this);

    for (jsize i = 0; i < n; i++) {
        jobject frontend = (*env)->GetObjectArrayElement(env, this, i);
        INTFrontend *sdkFrontend = IntFrontendTo(env, frontend);
        ret = g_list_append(ret, sdkFrontend);
    }

    return ret;
}










IntQrCodeType IntQrCode = {0};

void IntQrCodeLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntQrCode");
    IntQrCode.class = (*env)->NewGlobalRef(env, class);
    IntQrCode.local = (*env)->GetFieldID(env, class, "local", "Ljava/lang/String;");
    IntQrCode.external = (*env)->GetFieldID(env, class, "external", "Ljava/lang/String;");
    IntQrCode.password = (*env)->GetFieldID(env, class, "password", "Ljava/lang/String;");
    IntQrCode.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntQrCodeUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntQrCode.class);
}

jobject IntQrCodeFrom(JNIEnv *env, INTQRCode *sdkQrCode) {
    jstring local = (*env)->NewStringUTF(env, sdkQrCode->local);
    jstring external = (*env)->NewStringUTF(env, sdkQrCode->external);
    jstring password = (*env)->NewStringUTF(env, sdkQrCode->password);

    jobject this = (*env)->NewObject(env, IntQrCode.class, IntQrCode.init);
    (*env)->SetObjectField(env, this, IntQrCode.local, local);
    (*env)->SetObjectField(env, this, IntQrCode.external, external);
    (*env)->SetObjectField(env, this, IntQrCode.password, password);
    return this;
}

INTQRCode *IntQrCodeTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring local = (*env)->GetObjectField(env, this, IntQrCode.local);
    jstring external = (*env)->GetObjectField(env, this, IntQrCode.external);
    jstring password = (*env)->GetObjectField(env, this, IntQrCode.password);

    INTQRCode sdkQrCode = {0};
    sdkQrCode.local = GeJNIEnvGetStringUTFChars(env, local, NULL);
    sdkQrCode.external = GeJNIEnvGetStringUTFChars(env, external, NULL);
    sdkQrCode.password = GeJNIEnvGetStringUTFChars(env, password, NULL);
    INTQRCode *ret = int_qr_code_copy(&sdkQrCode);

    GeJNIEnvReleaseStringUTFChars(env, local, sdkQrCode.local);
    GeJNIEnvReleaseStringUTFChars(env, external, sdkQrCode.external);
    GeJNIEnvReleaseStringUTFChars(env, password, sdkQrCode.password);

    return ret;
}










IntTextType IntText = {0};

void IntTextLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntText");
    IntText.class = (*env)->NewGlobalRef(env, class);
    IntText.body = (*env)->GetFieldID(env, class, "body", "Ljava/lang/String;");
    IntText.created = (*env)->GetFieldID(env, class, "created", "J");
    IntText.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntTextUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntText.class);
}

jobject IntTextFrom(JNIEnv *env, INTText *sdkText) {
    jstring body = (*env)->NewStringUTF(env, sdkText->body);

    jobject this = (*env)->NewObject(env, IntText.class, IntText.init);
    (*env)->SetObjectField(env, this, IntText.body, body);
    (*env)->SetLongField(env, this, IntText.created, sdkText->created);
    return this;
}

INTText *IntTextTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring body = (*env)->GetObjectField(env, this, IntText.body);

    INTText sdkText = {0};
    sdkText.body = GeJNIEnvGetStringUTFChars(env, body, NULL);
    sdkText.created = (*env)->GetLongField(env, this, IntText.created);
    INTText *ret = int_text_copy(&sdkText);

    GeJNIEnvReleaseStringUTFChars(env, body, sdkText.body);

    return ret;
}

jobject IntTextsFrom(JNIEnv *env, GList *sdkTexts) {
    guint n = g_list_length(sdkTexts);
    jobjectArray this = (*env)->NewObjectArray(env, n, IntText.class, NULL);
    jsize i = 0;

    for (GList *sdkText = sdkTexts; sdkText != NULL; sdkText = sdkText->next) {
        jobject text = IntTextFrom(env, sdkText->data);
        (*env)->SetObjectArrayElement(env, this, i, text);
        i++;
    }

    return this;
}

GList *IntTextsTo(JNIEnv *env, jobjectArray this) {
    GList *ret = NULL;
    jsize n = (*env)->GetArrayLength(env, this);

    for (jsize i = 0; i < n; i++) {
        jobject text = (*env)->GetObjectArrayElement(env, this, i);
        INTText *sdkText = IntTextTo(env, text);
        ret = g_list_append(ret, sdkText);
    }

    return ret;
}










IntCallType IntCall = {0};

void IntCallLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntCall");
    IntCall.class = (*env)->NewGlobalRef(env, class);
    jfieldID ACTION_NONE = (*env)->GetStaticFieldID(env, class, "ACTION_NONE", "I");
    jfieldID ACTION_ANSWER = (*env)->GetStaticFieldID(env, class, "ACTION_ANSWER", "I");
    jfieldID ACTION_OPEN = (*env)->GetStaticFieldID(env, class, "ACTION_OPEN", "I");
    (*env)->SetStaticIntField(env, class, ACTION_NONE, (jint)INT_CALL_ACTION_NONE);
    (*env)->SetStaticIntField(env, class, ACTION_ANSWER, (jint)INT_CALL_ACTION_ANSWER);
    (*env)->SetStaticIntField(env, class, ACTION_OPEN, (jint)INT_CALL_ACTION_OPEN);
    IntCall.backend = (*env)->GetFieldID(env, class, "backend", "Ljava/lang/String;");
    IntCall.started = (*env)->GetFieldID(env, class, "started", "J");
    IntCall.ended = (*env)->GetFieldID(env, class, "ended", "J");
    IntCall.action = (*env)->GetFieldID(env, class, "action", "I");
    IntCall.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntCallUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntCall.class);
}

jobject IntCallFrom(JNIEnv *env, INTCall *sdkCall) {
    jstring backend = (*env)->NewStringUTF(env, sdkCall->backend);

    jobject this = (*env)->NewObject(env, IntCall.class, IntCall.init);
    (*env)->SetObjectField(env, this, IntCall.backend, backend);
    (*env)->SetLongField(env, this, IntCall.started, sdkCall->started);
    (*env)->SetLongField(env, this, IntCall.ended, sdkCall->ended);
    (*env)->SetIntField(env, this, IntCall.action, sdkCall->action);
    return this;
}

INTCall *IntCallTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring backend = (*env)->GetObjectField(env, this, IntCall.backend);

    INTCall sdkCall = {0};
    sdkCall.backend = GeJNIEnvGetStringUTFChars(env, backend, NULL);
    sdkCall.started = (*env)->GetLongField(env, this, IntCall.started);
    sdkCall.ended = (*env)->GetLongField(env, this, IntCall.ended);
    sdkCall.action = (*env)->GetIntField(env, this, IntCall.action);
    INTCall *ret = int_call_copy(&sdkCall);

    GeJNIEnvReleaseStringUTFChars(env, backend, sdkCall.backend);

    return ret;
}

jobjectArray IntCallsFrom(JNIEnv *env, GList *sdkCalls) {
    guint n = g_list_length(sdkCalls);
    jobjectArray this = (*env)->NewObjectArray(env, n, IntCall.class, NULL);
    jsize i = 0;

    for (GList *sdkCall = sdkCalls; sdkCall != NULL; sdkCall = sdkCall->next) {
        jobject call = IntCallFrom(env, sdkCall->data);
        (*env)->SetObjectArrayElement(env, this, i, call);
        i++;
    }

    return this;
}

GList *IntCallsTo(JNIEnv *env, jobjectArray this) {
    GList *ret = NULL;
    jsize n = (*env)->GetArrayLength(env, this);

    for (jsize i = 0; i < n; i++) {
        jobject call = (*env)->GetObjectArrayElement(env, this, i);
        INTCall *sdkCall = IntCallTo(env, call);
        ret = g_list_append(ret, sdkCall);
    }

    return ret;
}










IntFileType IntFile = {0};

void IntFileLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntFile");
    IntFile.class = (*env)->NewGlobalRef(env, class);
    IntFile.backend = (*env)->GetFieldID(env, class, "backend", "Ljava/lang/String;");
    IntFile.directory = (*env)->GetFieldID(env, class, "directory", "Ljava/lang/String;");
    IntFile.name = (*env)->GetFieldID(env, class, "name", "Ljava/lang/String;");
    IntFile.modified = (*env)->GetFieldID(env, class, "modified", "J");
    IntFile.duration = (*env)->GetFieldID(env, class, "duration", "D");
    IntFile.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntFileUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntFile.class);
}

jobject IntFileFrom(JNIEnv *env, INTFile *sdkFile) {
    jstring backend = (*env)->NewStringUTF(env, sdkFile->backend);
    jstring directory = (*env)->NewStringUTF(env, sdkFile->directory);
    jstring name = (*env)->NewStringUTF(env, sdkFile->name);

    jobject this = (*env)->NewObject(env, IntFile.class, IntFile.init);
    (*env)->SetObjectField(env, this, IntFile.backend, backend);
    (*env)->SetObjectField(env, this, IntFile.directory, directory);
    (*env)->SetObjectField(env, this, IntFile.name, name);
    (*env)->SetLongField(env, this, IntFile.modified, sdkFile->modified);
    (*env)->SetDoubleField(env, this, IntFile.duration, sdkFile->duration);
    return this;
}

INTFile *IntFileTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring backend = (*env)->GetObjectField(env, this, IntFile.backend);
    jstring directory = (*env)->GetObjectField(env, this, IntFile.directory);
    jstring name = (*env)->GetObjectField(env, this, IntFile.name);

    INTFile sdkFile = {0};
    sdkFile.backend = GeJNIEnvGetStringUTFChars(env, backend, NULL);
    sdkFile.directory = GeJNIEnvGetStringUTFChars(env, directory, NULL);
    sdkFile.name = GeJNIEnvGetStringUTFChars(env, name, NULL);
    sdkFile.modified = (*env)->GetLongField(env, this, IntFile.modified);
    sdkFile.duration = (*env)->GetDoubleField(env, this, IntFile.duration);
    INTFile *ret = int_file_copy(&sdkFile);

    GeJNIEnvReleaseStringUTFChars(env, backend, sdkFile.backend);
    GeJNIEnvReleaseStringUTFChars(env, directory, sdkFile.directory);
    GeJNIEnvReleaseStringUTFChars(env, name, sdkFile.name);

    return ret;
}

jobjectArray IntFilesFrom(JNIEnv *env, GList *sdkFiles) {
    guint n = g_list_length(sdkFiles);
    jobjectArray this = (*env)->NewObjectArray(env, n, IntFile.class, NULL);
    jsize i = 0;

    for (GList *sdkFile = sdkFiles; sdkFile != NULL; sdkFile = sdkFile->next) {
        jobject file = IntFileFrom(env, sdkFile->data);
        (*env)->SetObjectArrayElement(env, this, i, file);
        i++;
    }

    return this;
}

GList *IntFilesTo(JNIEnv *env, jobjectArray this) {
    GList *ret = NULL;
    jsize n = (*env)->GetArrayLength(env, this);

    for (jsize i = 0; i < n; i++) {
        jobject file = (*env)->GetObjectArrayElement(env, this, i);
        INTFile *sdkFile = IntFileTo(env, file);
        ret = g_list_append(ret, sdkFile);
    }

    return ret;
}










IntMessageType IntMessage = {0};

void IntMessageLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntMessage");
    IntMessage.class = (*env)->NewGlobalRef(env, class);
    IntMessage.right = (*env)->GetFieldID(env, class, "right", "Z");
    IntMessage.timestamp = (*env)->GetFieldID(env, class, "timestamp", "J");
    IntMessage.text = (*env)->GetFieldID(env, class, "text", "Llibrary/deintercom/IntText;");
    IntMessage.call = (*env)->GetFieldID(env, class, "call", "Llibrary/deintercom/IntCall;");
    IntMessage.file = (*env)->GetFieldID(env, class, "file", "Llibrary/deintercom/IntFile;");
    IntMessage.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntMessageUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntMessage.class);
}

jobject IntMessageFrom(JNIEnv *env, INTMessage *sdkMessage) {
    jobject text = IntTextFrom(env, sdkMessage->text);
    jobject call = IntCallFrom(env, sdkMessage->call);
    jobject file = IntFileFrom(env, sdkMessage->file);

    jobject this = (*env)->NewObject(env, IntMessage.class, IntMessage.init);
    (*env)->SetBooleanField(env, this, IntMessage.right, sdkMessage->right);
    (*env)->SetLongField(env, this, IntMessage.timestamp, sdkMessage->timestamp);
    (*env)->SetObjectField(env, this, IntMessage.text, text);
    (*env)->SetObjectField(env, this, IntMessage.call, call);
    (*env)->SetObjectField(env, this, IntMessage.file, file);
    return this;
}

INTMessage *IntMessageTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jobject text = (*env)->GetObjectField(env, this, IntMessage.text);
    jobject call = (*env)->GetObjectField(env, this, IntMessage.call);
    jobject file = (*env)->GetObjectField(env, this, IntMessage.file);

    INTMessage sdkMessage = {0};
    sdkMessage.right = (*env)->GetBooleanField(env, this, IntMessage.right);
    sdkMessage.timestamp = (*env)->GetLongField(env, this, IntMessage.timestamp);
    INTMessage *ret = int_message_copy(&sdkMessage);
    ret->text = IntTextTo(env, text);
    ret->call = IntCallTo(env, call);
    ret->file = IntFileTo(env, file);

    return ret;
}

jobjectArray IntMessagesFrom(JNIEnv *env, GList *sdkMessages) {
    guint n = g_list_length(sdkMessages);
    jobjectArray this = (*env)->NewObjectArray(env, n, IntMessage.class, NULL);
    jsize i = 0;

    for (GList *sdkMessage = sdkMessages; sdkMessage != NULL; sdkMessage = sdkMessage->next) {
        jobject message = IntMessageFrom(env, sdkMessage->data);
        (*env)->SetObjectArrayElement(env, this, i, message);
        i++;
    }

    return this;
}

GList *IntMessagesTo(JNIEnv *env, jobjectArray this) {
    GList *ret = NULL;
    jsize n = (*env)->GetArrayLength(env, this);

    for (jsize i = 0; i < n; i++) {
        jobject message = (*env)->GetObjectArrayElement(env, this, i);
        INTMessage *sdkMessage = IntMessageTo(env, message);
        ret = g_list_append(ret, sdkMessage);
    }

    return ret;
}










IntToneType IntTone = {0};

void IntToneLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntTone");
    IntTone.class = (*env)->NewGlobalRef(env, class);
    IntTone.ring = (*env)->GetFieldID(env, class, "ring", "Ljava/lang/String;");
    IntTone.busy = (*env)->GetFieldID(env, class, "busy", "Ljava/lang/String;");
    IntTone.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntToneUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntTone.class);
}

jobject IntToneFrom(JNIEnv *env, INTTone *sdkTone) {
    jstring ring = (*env)->NewStringUTF(env, sdkTone->ring);
    jstring busy = (*env)->NewStringUTF(env, sdkTone->busy);

    jobject this = (*env)->NewObject(env, IntTone.class, IntTone.init);
    (*env)->SetObjectField(env, this, IntTone.ring, ring);
    (*env)->SetObjectField(env, this, IntTone.busy, busy);
    return this;
}

INTTone *IntToneTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring ring = (*env)->GetObjectField(env, this, IntTone.ring);
    jstring busy = (*env)->GetObjectField(env, this, IntTone.busy);

    INTTone sdkTone = {0};
    sdkTone.ring = GeJNIEnvGetStringUTFChars(env, ring, NULL);
    sdkTone.busy = GeJNIEnvGetStringUTFChars(env, busy, NULL);
    INTTone *ret = int_tone_copy(&sdkTone);

    GeJNIEnvReleaseStringUTFChars(env, ring, sdkTone.ring);
    GeJNIEnvReleaseStringUTFChars(env, busy, sdkTone.busy);

    return ret;
}










IntPasswordType IntPassword = {0};

void IntPasswordLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntPassword");
    IntPassword.class = (*env)->NewGlobalRef(env, class);
    IntPassword.backend = (*env)->GetFieldID(env, class, "backend", "Ljava/lang/String;");
    IntPassword.name = (*env)->GetFieldID(env, class, "name", "Ljava/lang/String;");
    IntPassword.modified = (*env)->GetFieldID(env, class, "modified", "J");
    IntPassword.duration = (*env)->GetFieldID(env, class, "duration", "D");
    IntPassword.enabled = (*env)->GetFieldID(env, class, "enabled", "Z");
    IntPassword.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntPasswordUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntPassword.class);
}

jobject IntPasswordFrom(JNIEnv *env, INTPassword *sdkPassword) {
    jstring backend = (*env)->NewStringUTF(env, sdkPassword->backend);
    jstring name = (*env)->NewStringUTF(env, sdkPassword->name);

    jobject this = (*env)->NewObject(env, IntPassword.class, IntPassword.init);
    (*env)->SetObjectField(env, this, IntPassword.backend, backend);
    (*env)->SetObjectField(env, this, IntPassword.name, name);
    (*env)->SetLongField(env, this, IntPassword.modified, sdkPassword->modified);
    (*env)->SetDoubleField(env, this, IntPassword.duration, sdkPassword->duration);
    (*env)->SetBooleanField(env, this, IntPassword.enabled, sdkPassword->enabled);
    return this;
}

INTPassword *IntPasswordTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring backend = (*env)->GetObjectField(env, this, IntPassword.backend);
    jstring name = (*env)->GetObjectField(env, this, IntPassword.name);

    INTPassword sdkPassword = {0};
    sdkPassword.backend = GeJNIEnvGetStringUTFChars(env, backend, NULL);
    sdkPassword.name = GeJNIEnvGetStringUTFChars(env, name, NULL);
    sdkPassword.modified = (*env)->GetLongField(env, this, IntPassword.modified);
    sdkPassword.duration = (*env)->GetDoubleField(env, this, IntPassword.duration);
    sdkPassword.enabled = (*env)->GetBooleanField(env, this, IntPassword.enabled);
    INTPassword *ret = int_password_copy(&sdkPassword);

    GeJNIEnvReleaseStringUTFChars(env, backend, sdkPassword.backend);
    GeJNIEnvReleaseStringUTFChars(env, name, sdkPassword.name);

    return ret;
}

jobjectArray IntPasswordsFrom(JNIEnv *env, GList *sdkPasswords) {
    guint n = g_list_length(sdkPasswords);
    jobjectArray this = (*env)->NewObjectArray(env, n, IntPassword.class, NULL);
    jsize i = 0;

    for (GList *sdkPassword = sdkPasswords; sdkPassword != NULL; sdkPassword = sdkPassword->next) {
        jobject password = IntPasswordFrom(env, sdkPassword->data);
        (*env)->SetObjectArrayElement(env, this, i, password);
        i++;
    }

    return this;
}

GList *IntPasswordsTo(JNIEnv *env, jobjectArray this) {
    GList *ret = NULL;
    jsize n = (*env)->GetArrayLength(env, this);

    for (jsize i = 0; i < n; i++) {
        jobject password = (*env)->GetObjectArrayElement(env, this, i);
        INTPassword *sdkPassword = IntPasswordTo(env, password);
        ret = g_list_append(ret, sdkPassword);
    }

    return ret;
}
