//
// Created by Dan on 23.06.2022.
//

#ifndef LIBRARY_DEINTERCOM_INTSCHEMA_H
#define LIBRARY_DEINTERCOM_INTSCHEMA_H

#include "IntMain.h"










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID project;
    jfieldID token;
    jfieldID id;
    jfieldID a;
    jfieldID local;
    jfieldID external;
    jmethodID init;
} IntPushCallType;

extern IntPushCallType IntPushCall;

void IntPushCallLoad(JNIEnv *env);
void IntPushCallUnload(JNIEnv *env);

jobject IntPushCallFrom(JNIEnv *env, INTPushCall *sdkPushCall);
INTPushCall *IntPushCallTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID project;
    jfieldID token;
    jfieldID id;
    jfieldID local;
    jfieldID external;
    jmethodID init;
} IntPushIPType;

extern IntPushIPType IntPushIP;

void IntPushIPLoad(JNIEnv *env);
void IntPushIPUnload(JNIEnv *env);

jobject IntPushIPFrom(JNIEnv *env, INTPushIP *sdkPushIP);
INTPushIP *IntPushIPTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID project;
    jfieldID token;
    jfieldID backendId;
    jfieldID frontendId;
    jfieldID a;
    jmethodID init;
} IntPushAcceptType;

extern IntPushAcceptType IntPushAccept;

void IntPushAcceptLoad(JNIEnv *env);
void IntPushAcceptUnload(JNIEnv *env);

jobject IntPushAcceptFrom(JNIEnv *env, INTPushAccept *sdkPushAccept);
INTPushAccept *IntPushAcceptTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID id;
    jfieldID name;
    jfieldID local;
    jfieldID external;
    jmethodID init;
} IntBackendType;

extern IntBackendType IntBackend;

void IntBackendLoad(JNIEnv *env);
void IntBackendUnload(JNIEnv *env);

jobject IntBackendFrom(JNIEnv *env, INTBackend *sdkBackend);
INTBackend *IntBackendTo(JNIEnv *env, jobject this);

jobjectArray IntBackendsFrom(JNIEnv *env, GList *sdkBackends);
GList *IntBackendsTo(JNIEnv *env, jobjectArray this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID backend;
    jfieldID id;
    jfieldID os;
    jfieldID project;
    jfieldID token;
    jfieldID online;
    jfieldID lastSeen;
    jmethodID init;
} IntFrontendType;

extern IntFrontendType IntFrontend;

void IntFrontendLoad(JNIEnv *env);
void IntFrontendUnload(JNIEnv *env);

jobject IntFrontendFrom(JNIEnv *env, INTFrontend *sdkFrontend);
INTFrontend *IntFrontendTo(JNIEnv *env, jobject this);

jobject IntFrontendsFrom(JNIEnv *env, GList *sdkFrontends);
GList *IntFrontendsTo(JNIEnv *env, jobjectArray this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID local;
    jfieldID external;
    jfieldID password;
    jmethodID init;
} IntQrCodeType;

extern IntQrCodeType IntQrCode;

void IntQrCodeLoad(JNIEnv *env);
void IntQrCodeUnload(JNIEnv *env);

jobject IntQrCodeFrom(JNIEnv *env, INTQRCode *sdkQrCode);
INTQRCode *IntQrCodeTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID body;
    jfieldID created;
    jmethodID init;
} IntTextType;

extern IntTextType IntText;

void IntTextLoad(JNIEnv *env);
void IntTextUnload(JNIEnv *env);

jobject IntTextFrom(JNIEnv *env, INTText *sdkText);
INTText *IntTextTo(JNIEnv *env, jobject this);

jobject IntTextsFrom(JNIEnv *env, GList *sdkTexts);
GList *IntTextsTo(JNIEnv *env, jobjectArray this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID backend;
    jfieldID started;
    jfieldID ended;
    jfieldID action;
    jmethodID init;
} IntCallType;

extern IntCallType IntCall;

void IntCallLoad(JNIEnv *env);
void IntCallUnload(JNIEnv *env);

jobject IntCallFrom(JNIEnv *env, INTCall *sdkCall);
INTCall *IntCallTo(JNIEnv *env, jobject this);

jobjectArray IntCallsFrom(JNIEnv *env, GList *sdkCalls);
GList *IntCallsTo(JNIEnv *env, jobjectArray this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID backend;
    jfieldID directory;
    jfieldID name;
    jfieldID modified;
    jfieldID duration;
    jmethodID init;
} IntFileType;

extern IntFileType IntFile;

void IntFileLoad(JNIEnv *env);
void IntFileUnload(JNIEnv *env);

jobject IntFileFrom(JNIEnv *env, INTFile *sdkFile);
INTFile *IntFileTo(JNIEnv *env, jobject this);

jobjectArray IntFilesFrom(JNIEnv *env, GList *sdkFiles);
GList *IntFilesTo(JNIEnv *env, jobjectArray this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID right;
    jfieldID timestamp;
    jfieldID text;
    jfieldID call;
    jfieldID file;
    jmethodID init;
} IntMessageType;

extern IntMessageType IntMessage;

void IntMessageLoad(JNIEnv *env);
void IntMessageUnload(JNIEnv *env);

jobject IntMessageFrom(JNIEnv *env, INTMessage *sdkMessage);
INTMessage *IntMessageTo(JNIEnv *env, jobject this);

jobjectArray IntMessagesFrom(JNIEnv *env, GList *sdkMessages);
GList *IntMessagesTo(JNIEnv *env, jobjectArray this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID ring;
    jfieldID busy;
    jmethodID init;
} IntToneType;

extern IntToneType IntTone;

void IntToneLoad(JNIEnv *env);
void IntToneUnload(JNIEnv *env);

jobject IntToneFrom(JNIEnv *env, INTTone *sdkTone);
INTTone *IntToneTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID backend;
    jfieldID name;
    jfieldID modified;
    jfieldID duration;
    jfieldID enabled;
    jmethodID init;
} IntPasswordType;

extern IntPasswordType IntPassword;

void IntPasswordLoad(JNIEnv *env);
void IntPasswordUnload(JNIEnv *env);

jobject IntPasswordFrom(JNIEnv *env, INTPassword *sdkPassword);
INTPassword *IntPasswordTo(JNIEnv *env, jobject this);

jobjectArray IntPasswordsFrom(JNIEnv *env, GList *sdkPasswords);
GList *IntPasswordsTo(JNIEnv *env, jobjectArray this);

G_END_DECLS










#endif //LIBRARY_DEINTERCOM_INTSCHEMA_H
