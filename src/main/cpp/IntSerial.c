//
// Created by Dan on 19.03.2022.
//

#include "IntSerial.h"










IntSerialType IntSerial = {0};

void IntSerialLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntSerial");
    IntSerial.class = (*env)->NewGlobalRef(env, class);
    IntSerial.object = (*env)->GetFieldID(env, class, "object", "J");
    IntSerial.weak = (*env)->GetFieldID(env, class, "weak", "J");
    IntSerial.filename = (*env)->GetFieldID(env, class, "filename", "Ljava/lang/String;");
    IntSerial.listeners = (*env)->GetFieldID(env, class, "listeners", "Llibrary/deintercom/IntSerial$Listeners;");
    IntSerial.init = (*env)->GetMethodID(env, class, "<init>", "()V");
    IntSerial.call = (*env)->GetMethodID(env, class, "call", "(B)V");
}

void IntSerialUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntSerial.class);
}

JNIEXPORT void JNICALL Java_library_deintercom_IntSerial_close(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, IntSerial.object);
    if (object == 0) return;
    g_object_unref(GSIZE_TO_POINTER(object));
    (*env)->SetLongField(env, this, IntSerial.object, 0);

    jlong weak = (*env)->GetLongField(env, this, IntSerial.weak);
    (*env)->DeleteWeakGlobalRef(env, GSIZE_TO_POINTER(weak));
}

void IntSerialINTSerialCall(INTSerial *sdkSender, guint8 sdkA, gpointer sdkThis) {
    JNIEnv *env = NULL;
    if ((*GeJavaVM)->GetEnv(GeJavaVM, (void **)&env, JNI_VERSION_1_2) < JNI_OK) return;
    GeJNIEnvCallVoidMethod(env, sdkThis, IntSerial.call, sdkA);
}

JNIEXPORT jobject JNICALL Java_library_deintercom_IntSerial_serial(JNIEnv *env, jclass class, jstring filename) {
    jobject this = NULL;
    INTSerial *object = NULL;
    gchar *sdkFilename = GeJNIEnvGetStringUTFChars(env, filename, NULL);
    g_autoptr(GError) sdkError = NULL;

    if ((object = int_serial_new(sdkFilename, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        jobject listeners = (*env)->NewObject(env, IntSerialListeners.class, IntSerialListeners.init);

        this = (*env)->NewObject(env, class, IntSerial.init);
        (*env)->SetLongField(env, this, IntSerial.object, GPOINTER_TO_SIZE(object));
        (*env)->SetObjectField(env, this, IntSerial.filename, filename);
        (*env)->SetObjectField(env, this, IntSerial.listeners, listeners);

        jweak weak = (*env)->NewWeakGlobalRef(env, this);
        (*env)->SetLongField(env, this, IntSerial.weak, GPOINTER_TO_SIZE(weak));
        (void)g_signal_connect(object, "call", G_CALLBACK(IntSerialINTSerialCall), weak);
    }

    GeJNIEnvReleaseStringUTFChars(env, filename, sdkFilename);

    return this;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntSerial_answer(JNIEnv *env, jobject this, jbyte a) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntSerial.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_serial_answer(GSIZE_TO_POINTER(object), a, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntSerial_open(JNIEnv *env, jobject this, jbyte a) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntSerial.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_serial_open(GSIZE_TO_POINTER(object), a, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntSerial_readSync(JNIEnv *env, jobject this) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntSerial.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_serial_read_sync(GSIZE_TO_POINTER(object), &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}










IntSerialListenersType IntSerialListeners = {0};

void IntSerialListenersLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntSerial$Listeners");
    IntSerialListeners.class = (*env)->NewGlobalRef(env, class);
    IntSerialListeners.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntSerialListenersUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntSerialListeners.class);
}
