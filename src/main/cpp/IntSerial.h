//
// Created by Dan on 19.03.2022.
//

#ifndef LIBRARY_DEINTERCOM_INTSERIAL_H
#define LIBRARY_DEINTERCOM_INTSERIAL_H

#include "IntMain.h"










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID object;
    jfieldID weak;
    jfieldID filename;
    jfieldID listeners;
    jmethodID init;
    jmethodID call;
} IntSerialType;

extern IntSerialType IntSerial;

void IntSerialLoad(JNIEnv *env);
void IntSerialUnload(JNIEnv *env);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jmethodID init;
} IntSerialListenersType;

extern IntSerialListenersType IntSerialListeners;

void IntSerialListenersLoad(JNIEnv *env);
void IntSerialListenersUnload(JNIEnv *env);

G_END_DECLS










#endif //LIBRARY_DEINTERCOM_INTSERIAL_H
