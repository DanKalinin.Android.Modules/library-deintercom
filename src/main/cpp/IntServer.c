//
// Created by Dan on 19.05.2022.
//

#include "IntServer.h"

IntServerType IntServer = {0};

void IntServerLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntServer");
    IntServer.class = (*env)->NewGlobalRef(env, class);
    IntServer.object = (*env)->GetFieldID(env, class, "object", "J");
    IntServer.port = (*env)->GetFieldID(env, class, "port", "I");
    IntServer.database = (*env)->GetFieldID(env, class, "database", "Llibrary/deintercom/IntDatabase;");
    IntServer.serial = (*env)->GetFieldID(env, class, "serial", "Llibrary/deintercom/IntUsbSerial;");
    IntServer.frbClient = (*env)->GetFieldID(env, class, "frbClient", "Llibrary/deintercom/IntFrbClient;");
    IntServer.apnClient = (*env)->GetFieldID(env, class, "apnClient", "Llibrary/deintercom/IntApnClient;");
    IntServer.igd = (*env)->GetFieldID(env, class, "igd", "Llibrary/deintercom/IntIgd;");
    IntServer.astrDatabase = (*env)->GetFieldID(env, class, "astrDatabase", "Llibrary/asteriskext/AstrDatabase;");
    IntServer.agent = (*env)->GetFieldID(env, class, "agent", "Llibrary/deintercom/IntSipAgent;");
    IntServer.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntServerUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntServer.class);
}

JNIEXPORT void JNICALL Java_library_deintercom_IntServer_close(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, IntServer.object);
    if (object == 0) return;
    g_object_unref(GSIZE_TO_POINTER(object));
    (*env)->SetLongField(env, this, IntServer.object, 0);
}

JNIEXPORT jobject JNICALL Java_library_deintercom_IntServer_server(JNIEnv *env, jclass class, jint port, jobject database, jobject serial, jobject frbClient, jobject apnClient, jobject igd, jobject astrDatabase, jobject agent) {
    jobject this = NULL;
    INTSOEServer *object = NULL;
    jlong sdkConnection = (*env)->GetLongField(env, database, IntDatabase.object);
    jlong sdkSerial = (*env)->GetLongField(env, serial, IntUsbSerial.object);
    jlong sdkFrbClient = (*env)->GetLongField(env, frbClient, IntFrbClient.object);
    jlong sdkApnClient = (*env)->GetLongField(env, apnClient, IntApnClient.object);
    jlong sdkIgd = (*env)->GetLongField(env, igd, IntIgd.object);
    jlong sdkAstrConnection = (*env)->GetLongField(env, astrDatabase, AstrDatabase.object);
    jlong sdkAgent = (*env)->GetLongField(env, agent, IntSipAgent.object);
    g_autoptr(GError) sdkError = NULL;

    if ((object = int_soe_server_new(port, GSIZE_TO_POINTER(sdkConnection), GSIZE_TO_POINTER(sdkSerial), GSIZE_TO_POINTER(sdkFrbClient), GSIZE_TO_POINTER(sdkApnClient), GSIZE_TO_POINTER(sdkIgd), GSIZE_TO_POINTER(sdkAstrConnection), GSIZE_TO_POINTER(sdkAgent), &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        this = (*env)->NewObject(env, class, IntServer.init);
        (*env)->SetLongField(env, this, IntServer.object, GPOINTER_TO_SIZE(object));
        (*env)->SetIntField(env, this, IntServer.port, port);
        (*env)->SetObjectField(env, this, IntServer.database, database);
        (*env)->SetObjectField(env, this, IntServer.serial, serial);
        (*env)->SetObjectField(env, this, IntServer.frbClient, frbClient);
        (*env)->SetObjectField(env, this, IntServer.apnClient, apnClient);
        (*env)->SetObjectField(env, this, IntServer.igd, igd);
        (*env)->SetObjectField(env, this, IntServer.astrDatabase, astrDatabase);
        (*env)->SetObjectField(env, this, IntServer.agent, agent);
    }

    return this;
}
