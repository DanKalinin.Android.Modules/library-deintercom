//
// Created by Dan on 19.05.2022.
//

#ifndef LIBRARY_DEINTERCOM_INTSERVER_H
#define LIBRARY_DEINTERCOM_INTSERVER_H

#include "IntMain.h"
#include "IntDatabase.h"
#include "IntUsbSerial.h"
#include "IntFrbClient.h"
#include "IntApnClient.h"
#include "IntIgd.h"
#include "IntSipAgent.h"

G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID object;
    jfieldID port;
    jfieldID database;
    jfieldID serial;
    jfieldID frbClient;
    jfieldID apnClient;
    jfieldID igd;
    jfieldID astrDatabase;
    jfieldID agent;
    jmethodID init;
} IntServerType;

extern IntServerType IntServer;

void IntServerLoad(JNIEnv *env);
void IntServerUnload(JNIEnv *env);

G_END_DECLS

#endif //LIBRARY_DEINTERCOM_INTSERVER_H
