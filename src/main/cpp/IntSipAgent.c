//
// Created by Dan on 12.06.2023.
//

#include "IntSipAgent.h"

IntSipAgentType IntSipAgent = {0};

void IntSipAgentLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntSipAgent");
    IntSipAgent.class = (*env)->NewGlobalRef(env, class);
    IntSipAgent.object = (*env)->GetFieldID(env, class, "object", "J");
    IntSipAgent.device = (*env)->GetFieldID(env, class, "device", "Ljava/lang/String;");
    IntSipAgent.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntSipAgentUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntSipAgent.class);
}

JNIEXPORT void JNICALL Java_library_deintercom_IntSipAgent_close(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, IntSipAgent.object);
    if (object == 0) return;
    g_object_unref(GSIZE_TO_POINTER(object));
    (*env)->SetLongField(env, this, IntSipAgent.object, 0);
}

JNIEXPORT jobject JNICALL Java_library_deintercom_IntSipAgent_agent(JNIEnv *env, jclass class, jstring device, jobject tone) {
    jobject this = NULL;
    INTPJEAgent *object = NULL;
    gchar *sdkDevice = GeJNIEnvGetStringUTFChars(env, device, NULL);
    g_autoptr(INTTone) sdkTone = IntToneTo(env, tone);
    g_autoptr(GError) sdkError = NULL;

    if ((object = int_pje_agent_new(sdkDevice, sdkTone, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        this = (*env)->NewObject(env, class, IntSipAgent.init);
        (*env)->SetLongField(env, this, IntSipAgent.object, GPOINTER_TO_SIZE(object));
        (*env)->SetObjectField(env, this, IntSipAgent.device, device);
    }

    GeJNIEnvReleaseStringUTFChars(env, device, sdkDevice);

    return this;
}

JNIEXPORT jint JNICALL Java_library_deintercom_IntSipAgent_accountAdd(JNIEnv *env, jobject this, jstring host, jstring username, jstring password, jboolean stun) {
    jint ret = PJSUA_INVALID_ID;
    jlong object = (*env)->GetLongField(env, this, IntSipAgent.object);
    gchar *sdkHost = GeJNIEnvGetStringUTFChars(env, host, NULL);
    gchar *sdkUsername = GeJNIEnvGetStringUTFChars(env, username, NULL);
    gchar *sdkPassword = GeJNIEnvGetStringUTFChars(env, password, NULL);
    g_autoptr(GError) sdkError = NULL;

    if ((ret = int_pje_agent_account_add(GSIZE_TO_POINTER(object), sdkHost, sdkUsername, sdkPassword, stun, &sdkError)) == PJSUA_INVALID_ID) {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, host, sdkHost);
    GeJNIEnvReleaseStringUTFChars(env, username, sdkUsername);
    GeJNIEnvReleaseStringUTFChars(env, password, sdkPassword);

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntSipAgent_accountDelete(JNIEnv *env, jobject this, jint account) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntSipAgent.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_pje_agent_account_delete(GSIZE_TO_POINTER(object), account, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jint JNICALL Java_library_deintercom_IntSipAgent_callMake(JNIEnv *env, jobject this, jint account, jstring host, jstring extension) {
    jint ret = PJSUA_INVALID_ID;
    jlong object = (*env)->GetLongField(env, this, IntSipAgent.object);
    gchar *sdkHost = GeJNIEnvGetStringUTFChars(env, host, NULL);
    gchar *sdkExtension = GeJNIEnvGetStringUTFChars(env, extension, NULL);
    g_autoptr(GError) sdkError = NULL;

    if ((ret = int_pje_agent_call_make(GSIZE_TO_POINTER(object), account, sdkHost, sdkExtension, &sdkError)) == PJSUA_INVALID_ID) {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, host, sdkHost);
    GeJNIEnvReleaseStringUTFChars(env, extension, sdkExtension);

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntSipAgent_callHangup(JNIEnv *env, jobject this, jint call) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntSipAgent.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_pje_agent_call_hangup(GSIZE_TO_POINTER(object), call, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntSipAgent_callHangupIncoming(JNIEnv *env, jobject this) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntSipAgent.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_pje_agent_call_hangup_incoming(GSIZE_TO_POINTER(object), &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntSipAgent_audDevRefresh(JNIEnv *env, jobject this) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntSipAgent.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_pje_agent_aud_dev_refresh(GSIZE_TO_POINTER(object), &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}
