//
// Created by Dan on 12.06.2023.
//

#ifndef LIBRARY_DEINTERCOM_INTSIPAGENT_H
#define LIBRARY_DEINTERCOM_INTSIPAGENT_H

#include "IntMain.h"
#include "IntSchema.h"

G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID object;
    jfieldID device;
    jmethodID init;
} IntSipAgentType;

extern IntSipAgentType IntSipAgent;

void IntSipAgentLoad(JNIEnv *env);
void IntSipAgentUnload(JNIEnv *env);

G_END_DECLS

#endif //LIBRARY_DEINTERCOM_INTSIPAGENT_H
