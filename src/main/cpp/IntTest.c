//
// Created by Dan on 21.07.2022.
//

#include "IntTest.h"
#include <android/log.h>

typedef void TypeA;
typedef void TypeB;

void type_a_free(TypeA *self) {
    g_print("a\n");
}

void type_b_free(TypeB *self) {
    g_print("b\n");
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC(TypeA, type_a_free)
G_DEFINE_AUTOPTR_CLEANUP_FUNC(TypeB, type_b_free)

gboolean read_next(guchar *data, gint length, gint *transferred) {
    data[0] = 1;
    data[1] = 2;
    data[2] = 3;
    *transferred = 3;
    return TRUE;
}

void request_queued(SoupSession *session, SoupMessage *msg, gpointer user_data) {
    g_print("1\n");
}

void request_unqueued(SoupSession *session, SoupMessage *msg, gpointer user_data) {
    g_print("2\n");
}

void test_vosk_speaker() {
    FILE *wavin;
    char buf[3200];
    int nread, final;

    VoskModel *model = vosk_model_new("/data/data/a.intercom/files/vosk-model-small-ru-0.22");
    VoskSpkModel *spk_model = vosk_spk_model_new("/data/data/a.intercom/files/vosk-model-spk-0.4");
    VoskRecognizer *recognizer = vosk_recognizer_new_spk(model, 48000.0, spk_model);

    wavin = fopen("/data/data/a.intercom/files/dan-2.wav", "rb");
    fseek(wavin, 44, SEEK_SET);
    while (!feof(wavin)) {
        nread = fread(buf, 1, sizeof(buf), wavin);
        final = vosk_recognizer_accept_waveform(recognizer, buf, nread);
        if (final) {
            gchar *result = (gchar *)vosk_recognizer_result(recognizer) + 1000;
            printf("%s\n", vosk_recognizer_result(recognizer));
        } else {
//            printf("%s\n", vosk_recognizer_partial_result(recognizer));
        }
    }
    printf("%s\n", vosk_recognizer_final_result(recognizer));

    vosk_recognizer_free(recognizer);
    vosk_spk_model_free(spk_model);
    vosk_model_free(model);
}

JNIEXPORT void JNICALL Java_library_deintercom_IntTest_test(JNIEnv *env, jclass class) {
//    gpointer p = avd_avrdude_data;

//    test_vosk_speaker();

//    gchar *version = (gchar *)av_version_info();
//    g_print("version - %s\n", version);
//
//    g_autofree gchar *cmd = g_strdup_printf("ffmpeg -y -f s16le -ar 16k -ac 1 -ss 2.19 -to 5.82 -i /data/data/a.intercom/files/1.pcm /data/data/a.intercom/files/1.ogg");
//    g_auto(GStrv) argv = g_strsplit(cmd, " ", -1);
//    guint argc = g_strv_length(argv);
//    mpg_ffmpeg(argc, argv, NULL);

    return;

    SoupSession *session = soup_session_new();
    (void)g_signal_connect(session, "request-queued", G_CALLBACK(request_queued), NULL);
    (void)g_signal_connect(session, "request-unqueued", G_CALLBACK(request_unqueued), NULL);
    SoupMessage *message = soup_message_new("GET", "https://google.com");
    guint status = soup_session_send_message(session, message);
    g_print("OK\n");
    return;

    g_autoptr(TypeA) a = g_malloc(1);
    g_autoptr(TypeB) b = g_malloc(1);

    pj_pool_t *pool = pjsua_pool_create("%p", 1024, 1024);

    guint clock_rate = PJSUA_DEFAULT_CLOCK_RATE;
    guint samples_per_frame = clock_rate * 20 / 1000;
    pjmedia_port *tonegen = NULL;
    if (pjemedia_tonegen_create(pool, clock_rate, 1, samples_per_frame, 16, PJMEDIA_TONEGEN_LOOP, &tonegen, NULL) != PJ_SUCCESS) return;

    pjsua_conf_port_id source = PJSUA_INVALID_ID;
    if (pjesua_conf_add_port(pool, tonegen, &source, NULL) != PJ_SUCCESS) return;

    if (pjesua_conf_connect(source, 0, NULL) != PJ_SUCCESS) return;

    pjmedia_tone_desc tone = {0};
    tone.freq1 = 440;
    tone.freq2 = 480;
    tone.on_msec = 2000;
    tone.off_msec = 4000;
//    tone.freq1 = 400;
//    tone.freq2 = 450;
//    tone.on_msec = 400;
//    tone.off_msec = 200;
//    // Dial
//    tone.freq1 = 350;
//    tone.freq2 = 440;
//    tone.on_msec = 2000;
//    tone.off_msec = 4000;
//    // Busy
//    tone.freq1 = 480;
//    tone.freq2 = 620;
//    tone.on_msec = 500;
//    tone.off_msec = 500;
//    // Ring
//    tone.freq1 = 440;
//    tone.freq2 = 480;
//    tone.on_msec = 2000;
//    tone.off_msec = 4000;
//    // Congestion
//    tone.freq1 = 480;
//    tone.freq2 = 620;
//    tone.on_msec = 250;
//    tone.off_msec = 250;
//    // Callwaiting
//    tone.freq1 = 480;
//    tone.freq2 = 0;
//    tone.on_msec = 300;
//    tone.off_msec = 1000;
//    // Record
//    tone.freq1 = 1400;
//    tone.freq2 = 0;
//    tone.on_msec = 500;
//    tone.off_msec = 15000;
//    // Dialrecall
//    tone.freq1 = 350;
//    tone.freq2 = 440;
//    tone.on_msec = 100;
//    tone.off_msec = 100;
    if (pjemedia_tonegen_play(tonegen, 1, &tone, PJMEDIA_TONEGEN_LOOP, NULL) != PJ_SUCCESS) return;

//    pj_status_t status1 = pjemedia_tonegen_play(tonegen, 1, &tone, PJMEDIA_TONEGEN_LOOP, NULL);
//    pj_status_t status2 = pjemedia_tonegen_play(tonegen, 1, &tone, PJMEDIA_TONEGEN_LOOP, NULL);
//    pj_status_t status3 = pjmedia_tonegen_stop(tonegen);
//    g_print("ok\n");

    // Info
    pjmedia_tone_desc tones[] = {
            {950, 0, 330, 0, 0, 0},
            {1400, 0, 330, 0, 0, 0},
            {1800, 0, 330, 0, 0, 0}
    };

//    if (pjemedia_tonegen_play(tonegen, PJ_ARRAY_SIZE(tones), tones, PJMEDIA_TONEGEN_LOOP, NULL) != PJ_SUCCESS) return;

    pjmedia_tone_digit digits[] = {
            {'0', 100, 0, 0},
            {'1', 100, 0, 0},
            {'2', 100, 0, 0},
            {'3', 100, 0, 0},
            {'4', 100, 0, 0},
            {'5', 100, 0, 0},
            {'6', 100, 0, 0},
            {'7', 100, 0, 0},
            {'8', 100, 0, 0},
            {'9', 100, 0, 0},
            {'a', 100, 0, 0},
            {'b', 100, 0, 0},
            {'c', 100, 0, 0},
            {'d', 100, 0, 0},
            {'*', 100, 0, 0},
            {'#', 100, 0, 0}
    };

//    if (pjmedia_tonegen_play_digits(tonegen, PJ_ARRAY_SIZE(digits), digits, PJMEDIA_TONEGEN_LOOP) != PJ_SUCCESS) return;

////    pjsua_conf_disconnect(source, 0);
//    pjsua_conf_remove_port(source);
//    pjmedia_port_destroy(tonegen);
//    pj_pool_release(pool);

    g_print("ok\n");

    // *****
//    guchar data[10] = {0};
//    gint i = 0;
//    gint n = 0;
//    while (TRUE) {
//        guint8 pair[2] = {0};
//
//        for (int j = 0; j < 2; j++) {
//            if (i == n) {
//                do {
//                    if (!read_next(data, 10, &n)) return;
//                    i = 0;
//                } while (n == 0);
//            }
//
//            pair[j] = data[i++];
//        }
//
//        guint8 a = pair[0];
//        guint8 b = pair[1];
//
//        g_print("ok\n");
//    }

//    while (TRUE) {
//        guchar data[10];
//        gint n = 0;
//        read_next(data, 10, &n);
//        gint i = 0;
//        while (i < n) {
//            guint8 command = data[i++];
//
//            if (i == n) {
//                read_next(data, 10, &n);
//                i = 0;
//            }
//
//            guint8 argument = data[i++];
//
//            g_print("ok\n");
//        }
//    }

//    INTPJEAgent *agent = int_pje_agent_new(NULL, NULL, NULL);
//    pjsua_acc_id account1 = int_pje_agent_account_add(agent, "127.0.0.1", "backend", "1", NULL);
//    pjsua_acc_id account2 = int_pje_agent_account_add(agent, "127.0.0.1", "53ccf158-589e-4131-9679-1c16365a9cb8", "1", NULL);
////    pjsua_acc_id account2 = int_pje_agent_account_add(agent, "127.0.0.1", "da7cc624-7a5d-4f74-a0af-7e0f59b27f6f", "1", NULL);
//    pjsua_call_id call = int_pje_agent_call_make(agent, account2, "127.0.0.1", "6000", NULL);

//    //
//    pjmedia_snd_port_param param;
//    pjmedia_snd_port_param_default(&param);
//    if (pjmedia_aud_dev_default_param(3, &param.base) != PJ_SUCCESS) return;
////    param.base.flags = PJMEDIA_AUD_DEV_CAP_OUTPUT_ROUTE;
////    param.base.output_route = PJMEDIA_AUD_DEV_ROUTE_CUSTOM | 1;
//
//    pjsua_ext_snd_dev *dev = NULL;
//    if (pjsua_ext_snd_dev_create(&param, &dev) != PJ_SUCCESS) return;
//
//    pjsua_conf_port_id dev_port = pjsua_ext_snd_dev_get_conf_port(dev);
////    if (pjsua_conf_connect(dev_port, 0) != PJ_SUCCESS) return;
////    if (pjsua_conf_connect(0, dev_port) != PJ_SUCCESS) return;
//    //

//    pjsua_player_id player_id = PJSUA_INVALID_ID;
//    pj_str_t filename = pj_str("/data/data/a.intercom/files/1.wav");
//    if (pjsua_player_create(&filename, 0, &player_id) != PJ_SUCCESS) return;
//    pjsua_conf_port_id player_port = pjsua_player_get_conf_port(player_id);
//    if (pjsua_conf_connect(player_port, 0) != PJ_SUCCESS) return;

//    guint n = 64;
//    pjmedia_aud_dev_info infos[n];
//    pjsua_enum_aud_devs(infos, &n);
//    g_print("1\n");

// *****
//    gint n = pjmedia_snd_get_dev_count();
//    for (gint i = 0; i < n; i++) {
//        pjmedia_aud_dev_info info = {0};
//        if (pjmedia_aud_dev_get_info(i, &info) != PJ_SUCCESS) continue;
//        g_print("ok\n");
//    }
// *****
//    guint n = 64;
//    pjmedia_aud_dev_info infos[n];
//    if (pjesua_enum_aud_devs(infos, &n, NULL) != PJ_SUCCESS) return;
//
//    for (guint i = 0; i < n; i++) {
//        pjmedia_aud_dev_info info = infos[i];
//        // "Default"
//        // "Builtin Speaker"
//        // "USB Headset - USB-Audio - USB PnP Sound Device"
//        // "Bluetooth SCO - Наушники AirPods (Dan) - Find My"
//        if (g_str_has_prefix(info.name, "Default") || g_str_has_prefix(info.name, "Builtin Speaker")) {
//            if (info.output_count == 0) continue;
//
//            pjmedia_snd_port_param param = {0};
//            pjmedia_snd_port_param_default(&param);
//            if (pjemedia_aud_dev_default_param(i, &param.base, NULL) != PJ_SUCCESS) continue;
//            param.base.clock_rate = PJSUA_DEFAULT_CLOCK_RATE;
//            param.base.samples_per_frame = param.base.clock_rate * 20 / 1000;
//
//            pjsua_ext_snd_dev *dev = NULL;
//            if (pjesua_ext_snd_dev_create(&param, &dev, NULL) != PJ_SUCCESS) continue;
//            pjsua_conf_port_id port = pjsua_ext_snd_dev_get_conf_port(dev);
////            if (pjsua_ext_snd_dev_destroy(dev) != PJ_SUCCESS) continue;
//
//            pjsua_player_id player_id = PJSUA_INVALID_ID;
//            pj_str_t filename = g_str_has_prefix(info.name, "Default") ? pj_str("/data/data/a.intercom/files/1.wav") : pj_str("/data/data/a.intercom/files/2.wav");
//            if (pjsua_player_create(&filename, 0, &player_id) != PJ_SUCCESS) continue;
//            pjsua_conf_port_id player_port = pjsua_player_get_conf_port(player_id);
//            if (pjsua_conf_connect(player_port, port) != PJ_SUCCESS) continue;
//
//            g_print("ok\n");
//        }
//    }
// *****

//    for (unsigned i = 0; i<count ;++i) {
//        AudioDevInfo *dev_info = new AudioDevInfo;
//        dev_info->fromPj(pj_info[i]);
//        audioDevList.push_back(dev_info);
//    }


//    pjmedia_aud_dev_info pj_info[MAX_DEV_COUNT];
//    unsigned count = MAX_DEV_COUNT;
//
//    PJSUA2_CHECK_EXPR( pjsua_enum_aud_devs(pj_info, &count) );

//    int dev_count;
//    pjmedia_aud_dev_index dev_idx;
//    pj_status_t status;
//    dev_count = pjmedia_aud_dev_count();
//    printf("Got %d audio devices\n", dev_count);
//    for (dev_idx=0; dev_idx<dev_count; ++i) {
//        pjmedia_aud_dev_info info;
//        status = pjmedia_aud_dev_get_info(dev_idx, &info);
//        printf("%d. %s (in=%d, out=%d)\n",
//               dev_idx, info.name,
//               info.input_count, info.output_count);
//    }

//    guint n = pjmedia_aud_dev_count();
//    g_print("1\n");

//    pjmedia_aud_dev_info info;
//    pjmedia_aud_dev_get_info(0, &info);
//    g_print("1\n");

//    pjmedia_aud_subsys *subsys = pjmedia_get_aud_subsys();
//    g_print("1\n");

//    INTPJEEndpoint *endpoint = int_pje_endpoint_new(NULL);
//    INTPJEAccount *account1 = int_pje_account_new("127.0.0.1", "backend", "1", NULL);
//    INTPJEAccount *account2 = int_pje_account_new("127.0.0.1", "53ccf158-589e-4131-9679-1c16365a9cb8", "1", NULL);
//    GError *error = NULL;
//    INTPJECall *call = int_pje_call_new(account2, "127.0.0.1", "6000", &error);
//    g_print("1\n");

//    gchar *tmp = (gchar *)g_get_tmp_dir();
//    g_print("%s", tmp);
//
//    guint n = pjmedia_aud_dev_count();
//    g_print("n - %u\n", n);
//
//    pjmedia_aud_subsys *subsys = pjmedia_get_aud_subsys();
//    g_print("1\n");
//
//    pjmedia_aud_dev_info info = {0};
//    pj_status_t r = pjmedia_aud_dev_get_info(0, &info);
//    g_print("1\n");

//    g_auto(MPGFormatContext) context = NULL;
//    if (mpg_format_open_input(&context, "/data/data/a.intercom/files/1/Numb.mp3", NULL, NULL, NULL) < 0) return;
//    if (mpg_format_find_stream_info(context, NULL, NULL) < 0) return;
//    gdouble duration = (gdouble)context->duration / (gdouble)AV_TIME_BASE;

//    gchar *u = g_build_filename("a", "b", NULL);

//    SoupURI *uri = soup_uri_new("http://127.0.0.1/upload?path=a/b/c");
//    GHashTable *query = soup_form_decode(uri->query);
//    gchar *path = g_hash_table_lookup(query, "path");

//    GStrv components = g_strsplit("a/b/c", "/", 2);
//    gchar *a = components[0];
//    gchar *b = components[1];
//
//    g_print("xxx\n");

//    g_autoptr(GFile) dir = g_file_new_for_path("/data/data/a.intercom/files/1");
//    g_autoptr(GFileEnumerator) enumerator = g_file_enumerate_children(dir, "*", G_FILE_QUERY_INFO_NONE, NULL, NULL);
//
//    GFileInfo *info = NULL;
//    while ((info = g_file_enumerator_next_file(enumerator, NULL, NULL)) != NULL) {
//        gchar **attributes = g_file_info_list_attributes(info, NULL);
//        gchar *attribute = NULL;
//
//        for (attribute = *attributes; attribute != NULL; attribute = *attributes) {
//            g_print("attribute\n");
//            attributes++;
//        }
//
//        g_print("xxx\n");
//    }
//
//    g_print("xxx\n");

//    g_autoptr(GDir) dir = NULL;
//    if ((dir = g_dir_open("/data/data/a.intercom/files/1", 0, NULL)) == NULL) return;
//
//    // g_dir_read_name()
//    // -> null - No more files | errno - Check
//
//    gchar *name = NULL;
//    while ((name = (gchar *)g_dir_read_name(dir)) != NULL) {
//        g_print("name\n");
//    }
//
//    g_print("xxx\n");

//    INTQRCode qr_code1 = {0};
//    qr_code1.local = "192.168.0.1";
//    qr_code1.external = "192.168.0.2";
//    qr_code1.username = "F2C24625-8BF5-4FD5-B387-05F9CBFA8719";
//    qr_code1.password = "12345678";
//
//    g_autoptr(GError) error1 = NULL;
//
//    gchar *token = int_jwt_qr_code_encode(&qr_code1, "a.intercom", &error1);
//
//    g_autoptr(GError) error2 = NULL;
//
//    g_autoptr(INTQRCode) qr_code2 = int_jwt_qr_code_decode(token, "a.intercom", &error2);




//    SoupURI *uri_local = soup_uri_new("http://127.0.0.1:60000");
//    INTSOESession *local = int_soe_session_new(uri_local, NULL);
//
//    SoupURI *uri_external = soup_uri_new("http://127.0.0.1:60000");
//    INTSOESession *external = int_soe_session_new(uri_external, NULL);
//
//    INTPinger *pinger = int_pinger_new(local, external);

//    gchar *a = g_get_user_name();
//    // "u0_a155"
//    gchar *b = g_get_real_name();
//    // "Unknown"
//    gchar *c = g_get_host_name();
//    // "localhost"

//    gchar *a = soup_auth_domain_digest_encode_password("dan", "intercom", "1");
//    "4e0e5079a176f74a5effe297934a0b9a"
//    "4e0e5079a176f74a5effe297934a0b9a"

//    gint32 n = g_random_int_range(10, 20);
//
//    g_autoptr(gnutls_datum_t) key1 = g_new0(gnutls_datum_t, 1);
//    gnutls_key_generate(key1, n);
//    gchar *k1 = (gchar *)key1->data;
//    gsize l1 = strlen(k1);
//
//    g_autoptr(gnutls_datum_t) key2 = g_new0(gnutls_datum_t, 1);
//    gnutls_key_generate(key2, n);
//    gchar *k2 = (gchar *)key2->data;
//    gsize l2 = strlen(k2);

//    g_autoptr(GError) error = NULL;
//    GTlsCertificate *certificate = int_certificate_load(&error);

//    g_print("xxx\n");
}

JNIEXPORT jint JNICALL Java_library_deintercom_IntTest_fork(JNIEnv *env, jclass class) {
    jint ret = fork();
    return ret;
}
