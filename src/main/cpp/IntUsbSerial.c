//
// Created by Dan on 05.04.2022.
//

#include "IntUsbSerial.h"










IntUsbSerialType IntUsbSerial = {0};

void IntUsbSerialLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntUsbSerial");
    IntUsbSerial.class = (*env)->NewGlobalRef(env, class);
    IntUsbSerial.object = (*env)->GetFieldID(env, class, "object", "J");
    IntUsbSerial.weak = (*env)->GetFieldID(env, class, "weak", "J");
    IntUsbSerial.listeners = (*env)->GetFieldID(env, class, "listeners", "Llibrary/deintercom/IntUsbSerial$Listeners;");
    IntUsbSerial.init = (*env)->GetMethodID(env, class, "<init>", "()V");
    IntUsbSerial.call = (*env)->GetMethodID(env, class, "call", "(B)V");
}

void IntUsbSerialUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntUsbSerial.class);
}

JNIEXPORT void JNICALL Java_library_deintercom_IntUsbSerial_close(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, IntUsbSerial.object);
    if (object == 0) return;
    g_object_unref(GSIZE_TO_POINTER(object));
    (*env)->SetLongField(env, this, IntUsbSerial.object, 0);

    jlong weak = (*env)->GetLongField(env, this, IntUsbSerial.weak);
    (*env)->DeleteWeakGlobalRef(env, GSIZE_TO_POINTER(weak));
}

void IntUsbSerialINTUSBESerialCall(INTUSBESerial *sdkSender, guint8 sdkA, gpointer sdkThis) {
    JNIEnv *env = NULL;
    if ((*GeJavaVM)->GetEnv(GeJavaVM, (void **)&env, JNI_VERSION_1_2) < JNI_OK) return;
    GeJNIEnvCallVoidMethod(env, sdkThis, IntUsbSerial.call, sdkA);
}

JNIEXPORT jobject JNICALL Java_library_deintercom_IntUsbSerial_serial(JNIEnv *env, jclass class, jint fd) {
    jobject this = NULL;
    INTUSBESerial *object = NULL;
    g_autoptr(GError) sdkError = NULL;

    if ((object = int_usbe_serial_new(fd, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        jobject listeners = (*env)->NewObject(env, IntUsbSerialListeners.class, IntUsbSerialListeners.init);

        this = (*env)->NewObject(env, class, IntUsbSerial.init);
        (*env)->SetLongField(env, this, IntUsbSerial.object, GPOINTER_TO_SIZE(object));
        (*env)->SetObjectField(env, this, IntUsbSerial.listeners, listeners);

        jweak weak = (*env)->NewWeakGlobalRef(env, this);
        (*env)->SetLongField(env, this, IntUsbSerial.weak, GPOINTER_TO_SIZE(weak));
        (void)g_signal_connect(object, "call", G_CALLBACK(IntUsbSerialINTUSBESerialCall), weak);
    }

    return this;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntUsbSerial_answer(JNIEnv *env, jobject this, jbyte a) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntUsbSerial.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_usbe_serial_answer(GSIZE_TO_POINTER(object), a, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntUsbSerial_open(JNIEnv *env, jobject this, jbyte a) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntUsbSerial.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_usbe_serial_open(GSIZE_TO_POINTER(object), a, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_deintercom_IntUsbSerial_readSync(JNIEnv *env, jobject this) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, IntUsbSerial.object);
    g_autoptr(GError) sdkError = NULL;

    if (int_usbe_serial_read_sync(GSIZE_TO_POINTER(object), &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}










IntUsbSerialListenersType IntUsbSerialListeners = {0};

void IntUsbSerialListenersLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/deintercom/IntUsbSerial$Listeners");
    IntUsbSerialListeners.class = (*env)->NewGlobalRef(env, class);
    IntUsbSerialListeners.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void IntUsbSerialListenersUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, IntUsbSerialListeners.class);
}
