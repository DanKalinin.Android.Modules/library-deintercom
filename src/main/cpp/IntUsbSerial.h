//
// Created by Dan on 05.04.2022.
//

#ifndef LIBRARY_DEINTERCOM_INTUSBSERIAL_H
#define LIBRARY_DEINTERCOM_INTUSBSERIAL_H

#include "IntMain.h"










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID object;
    jfieldID weak;
    jfieldID listeners;
    jmethodID init;
    jmethodID call;
} IntUsbSerialType;

extern IntUsbSerialType IntUsbSerial;

void IntUsbSerialLoad(JNIEnv *env);
void IntUsbSerialUnload(JNIEnv *env);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jmethodID init;
} IntUsbSerialListenersType;

extern IntUsbSerialListenersType IntUsbSerialListeners;

void IntUsbSerialListenersLoad(JNIEnv *env);
void IntUsbSerialListenersUnload(JNIEnv *env);

G_END_DECLS










#endif //LIBRARY_DEINTERCOM_INTUSBSERIAL_H
