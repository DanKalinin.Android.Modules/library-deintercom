package library.deintercom;

import android.os.Handler;
import android.os.Looper;
import java.io.Closeable;
import java.util.ArrayList;
import library.apnext.ApnCredential;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class IntApnClient extends LjlObject implements Closeable {
    public interface Listener {
        void pushDone(IntApnClient sender, String id, GeException exception);
    }

    public static class Listeners extends ArrayList<Listener> implements Listener {
        @Override
        public void pushDone(IntApnClient sender, String id, GeException exception) {
            for (Listener listener : this) {
                listener.pushDone(sender, id, exception);
            }
        }
    }

    private long object;
    private long weak;
    public String base;
    public ApnCredential credential;
    public Listeners listeners;

    @Override
    protected void finalize() throws Throwable {
        close();
    }

    @Override
    public native void close();

    public void pushDone(String id, GeException exception) {
        new Handler(Looper.getMainLooper()).post(() -> listeners.pushDone(this, id, exception));
    }

    public static native IntApnClient client(String base, ApnCredential credential) throws GeException;

    public native void abort();

    // PushCall

    public native long pushCall(IntPushCall pushCall) throws GeException;

    // PushIP

    public native long pushIP(IntPushIP pushIP) throws GeException;

    // PushAccept

    public native long pushAccept(IntPushAccept pushAccept) throws GeException;
}
