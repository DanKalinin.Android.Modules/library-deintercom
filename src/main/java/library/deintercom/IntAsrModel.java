package library.deintercom;

import android.os.Handler;
import android.os.Looper;
import java.io.Closeable;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class IntAsrModel extends LjlObject implements Closeable {
    private long object;
    public String directory;

    @Override
    protected void finalize() throws Throwable {
        close();
    }

    @Override
    public native void close();

    // Model

    public static native IntAsrModel modelSync(String directory) throws GeException;

    public interface ModelAsyncCallback {
        void callback(IntAsrModel model, GeException exception);
    }

    public static void modelAsync(String directory, ModelAsyncCallback callback) {
        new Thread(() -> {
            try {
                IntAsrModel model = modelSync(directory);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(model, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }
}
