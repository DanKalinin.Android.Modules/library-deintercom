package library.deintercom;

import android.os.Handler;
import android.os.Looper;
import java.io.Closeable;
import library.glibext.GeException;
import library.java.lang.LjlObject;
import library.voskext.VskResult;

public class IntAsrRecognizer extends LjlObject implements Closeable {
    private long object;
    public IntAsrModel model;
    public float sampleRate;

    @Override
    protected void finalize() throws Throwable {
        close();
    }

    @Override
    public native void close();

    public static native IntAsrRecognizer recognizer(IntAsrModel model, float sampleRate) throws GeException;

    // Recognize

    public native VskResult recognizeSync(short[] data, int n) throws GeException;

    public interface RecognizeAsyncCallback {
        void callback(VskResult result, GeException exception);
    }

    public void recognizeAsync(short[] data, int n, RecognizeAsyncCallback callback) {
        new Thread(() -> {
            try {
                VskResult result = recognizeSync(data, n);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(result, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }
}
