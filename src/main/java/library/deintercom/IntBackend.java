package library.deintercom;

import library.java.lang.LjlObject;

public class IntBackend extends LjlObject {
    public String id;
    public String name;
    public String local;
    public String external;
}
