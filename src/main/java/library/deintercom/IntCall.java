package library.deintercom;

import library.java.lang.LjlObject;

public class IntCall extends LjlObject {
    public static int ACTION_NONE;
    public static int ACTION_ANSWER;
    public static int ACTION_OPEN;

    public String backend;
    public long started;
    public long ended;
    public int action;
}
