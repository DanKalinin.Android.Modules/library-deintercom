package library.deintercom;

import android.os.Handler;
import android.os.Looper;
import java.io.Closeable;
import java.util.ArrayList;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class IntClient extends LjlObject implements Closeable {
    public interface Listener {
        void call(IntClient sender, byte a);
        void ip(IntClient sender, String local, String external);
        void accept(IntClient sender, String id, byte a);
        void progress(IntClient sender, long current, long total, int tag);
    }

    public static class Listeners extends ArrayList<Listener> implements Listener {
        @Override
        public void call(IntClient sender, byte a) {
            for (Listener listener : this) {
                listener.call(sender, a);
            }
        }

        @Override
        public void ip(IntClient sender, String local, String external) {
            for (Listener listener : this) {
                listener.ip(sender, local, external);
            }
        }

        @Override
        public void accept(IntClient sender, String id, byte a) {
            for (Listener listener : this) {
                listener.accept(sender, id, a);
            }
        }

        @Override
        public void progress(IntClient sender, long current, long total, int tag) {
            for (Listener listener : this) {
                listener.progress(sender, current, total, tag);
            }
        }
    }

    private long object;
    private long weak;
    public String base;
    public String username;
    public String password;
    public IntDatabase database;
    public Listeners listeners;

    @Override
    protected void finalize() throws Throwable {
        close();
    }

    @Override
    public native void close();

    public void call(byte a) {
        new Handler(Looper.getMainLooper()).post(() -> listeners.call(this, a));
    }

    public void ip(String local, String external) {
        new Handler(Looper.getMainLooper()).post(() -> listeners.ip(this, local, external));
    }

    public void accept(String id, byte a) {
        new Handler(Looper.getMainLooper()).post(() -> listeners.accept(this, id, a));
    }

    public void progress(long current, long total, int tag) {
        new Handler(Looper.getMainLooper()).post(() -> listeners.progress(this, current, total, tag));
    }

    public static native IntClient client(String base, String username, String password, IntDatabase database);

    public native void abort();
    public native void cancel(int tag);
    public native void online();
    public native void offline();

    // Answer

    public native boolean answer(byte a) throws GeException;

    public interface AnswerAsyncCallback {
        void callback(GeException exception);
    }

    public void answerAsync(byte a, AnswerAsyncCallback callback) {
        new Thread(() -> {
            try {
                answer(a);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }

    // Open

    public native boolean open(byte a) throws GeException;

    public interface OpenAsyncCallback {
        void callback(GeException exception);
    }

    public void openAsync(byte a, OpenAsyncCallback callback) {
        new Thread(() -> {
            try {
                open(a);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }

    // Read

    public native boolean readSync() throws GeException;

    public interface ReadAsyncCallback {
        void callback(GeException exception);
    }

    public void readAsync(ReadAsyncCallback callback) {
        new Thread(() -> {
            try {
                readSync();
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }

    // Ping

    public native boolean pingSync() throws GeException;

    public interface PingAsyncCallback {
        void callback(GeException exception);
    }

    public void pingAsync(PingAsyncCallback callback) {
        new Thread(() -> {
            try {
                pingSync();
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }

    // Register

    public native IntBackend registerSync(IntFrontend frontend) throws GeException;

    public interface RegisterAsyncCallback {
        void callback(IntBackend backend, GeException exception);
    }

    public void registerAsync(IntFrontend frontend, RegisterAsyncCallback callback) {
        new Thread(() -> {
            try {
                IntBackend backend = registerSync(frontend);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(backend, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // Unregister

    public native boolean unregister() throws GeException;

    public interface UnregisterAsyncCallback {
        void callback(GeException exception);
    }

    public void unregisterAsync(UnregisterAsyncCallback callback) {
        new Thread(() -> {
            try {
                unregister();
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }

    // QrCode

    public native IntQrCode qrCodeSync() throws GeException;

    public interface QrCodeAsyncCallback {
        void callback(IntQrCode qrCode, GeException exception);
    }

    public void qrCodeAsync(QrCodeAsyncCallback callback) {
        new Thread(() -> {
            try {
                IntQrCode qrCode = qrCodeSync();
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(qrCode, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // Calls

    public native IntCall[] callsSync(long started) throws GeException;

    public interface CallsAsyncCallback {
        void callback(IntCall[] calls, GeException exception);
    }

    public void callsAsync(long started, CallsAsyncCallback callback) {
        new Thread(() -> {
            try {
                IntCall[] calls = callsSync(started);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(calls, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // Upload

    public native boolean uploadSync(String name, String file, int tag) throws GeException;

    public interface UploadAsyncCallback {
        void callback(GeException exception);
    }

    public void uploadAsync(String name, String file, int tag, UploadAsyncCallback callback) {
        new Thread(() -> {
            try {
                uploadSync(name, file, tag);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }

    // Download

    public native boolean downloadSync(String name, String file, int tag) throws GeException;

    public interface DownloadAsyncCallback {
        void callback(GeException exception);
    }

    public void downloadAsync(String name, String file, int tag, DownloadAsyncCallback callback) {
        new Thread(() -> {
            try {
                downloadSync(name, file, tag);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }

    // Files

    public native IntFile[] filesSync(String directory, long modified) throws GeException;

    public interface FilesAsyncCallback {
        void callback(IntFile[] files, GeException exception);
    }

    public void filesAsync(String directory, long modified, FilesAsyncCallback callback) {
        new Thread(() -> {
            try {
                IntFile[] files = filesSync(directory, modified);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(files, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // Passwords

    public native IntPassword[] passwords() throws GeException;

    public interface PasswordsAsyncCallback {
        void callback(IntPassword[] passwords, GeException exception);
    }

    public void passwordsAsync(PasswordsAsyncCallback callback) {
        new Thread(() -> {
            try {
                IntPassword[] passwords = passwords();
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(passwords, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // PasswordUpload

    public native IntPassword passwordUpload(String name, String file, int tag) throws GeException;

    public interface PasswordUploadAsyncCallback {
        void callback(IntPassword password, GeException exception);
    }

    public void passwordUploadAsync(String name, String file, int tag, PasswordUploadAsyncCallback callback) {
        new Thread(() -> {
            try {
                IntPassword password = passwordUpload(name, file, tag);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(password, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // PasswordEnable

    public native boolean passwordEnable(String name, boolean enabled) throws GeException;

    public interface PasswordEnableAsyncCallback {
        void callback(GeException exception);
    }

    public void passwordEnableAsync(String name, boolean enabled, PasswordEnableAsyncCallback callback) {
        new Thread(() -> {
            try {
                passwordEnable(name, enabled);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }

    // PasswordDelete

    public native boolean passwordDelete(String name) throws GeException;

    public interface PasswordDeleteAsyncCallback {
        void callback(GeException exception);
    }

    public void passwordDeleteAsync(String name, PasswordDeleteAsyncCallback callback) {
        new Thread(() -> {
            try {
                passwordDelete(name);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }

    // Frontends

    public native IntFrontend[] frontends() throws GeException;

    public interface FrontendsAsyncCallback {
        void callback(IntFrontend[] frontends, GeException exception);
    }

    public void frontendsAsync(FrontendsAsyncCallback callback) {
        new Thread(() -> {
            try {
                IntFrontend[] frontends = frontends();
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(frontends, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // FrontendDelete

    public native boolean frontendDelete(String id) throws GeException;

    public interface FrontendDeleteAsyncCallback {
        void callback(GeException exception);
    }

    public void frontendDeleteAsync(String id, FrontendDeleteAsyncCallback callback) {
        new Thread(() -> {
            try {
                frontendDelete(id);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }

    // CallsV1

    public native IntCall[] callsV1Sync() throws GeException;

    public interface CallsV1AsyncCallback {
        void callback(IntCall[] calls, GeException exception);
    }

    public void callsV1Async(CallsV1AsyncCallback callback) {
        new Thread(() -> {
            try {
                IntCall[] calls = callsV1Sync();
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(calls, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // FilesV1

    public native IntFile[] filesV1Sync(String directory) throws GeException;

    public interface FilesV1AsyncCallback {
        void callback(IntFile[] files, GeException exception);
    }

    public void filesV1Async(String directory, FilesV1AsyncCallback callback) {
        new Thread(() -> {
            try {
                IntFile[] files = filesV1Sync(directory);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(files, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }
}
