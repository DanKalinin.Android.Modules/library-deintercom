package library.deintercom;

import android.os.Handler;
import android.os.Looper;
import java.io.Closeable;
import java.util.ArrayList;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class IntDatabase extends LjlObject implements Closeable {
    public interface Listener {
        void commit(IntDatabase sender);
    }

    public static class Listeners extends ArrayList<Listener> implements Listener {
        @Override
        public void commit(IntDatabase sender) {
            for (Listener listener : this) {
                listener.commit(sender);
            }
        }
    }

    private long object;
    private long weak;
    public String file;
    public Listeners listeners;

    @Override
    protected void finalize() throws Throwable {
        close();
    }

    @Override
    public native void close();

    public void commit() {
        new Handler(Looper.getMainLooper()).post(() -> listeners.commit(this));
    }

    public static native IntDatabase database(String file) throws GeException;

    public native int[] selectInt(String tail) throws GeException;
    public native String[] selectText(String tail) throws GeException;

    public native boolean backendUpsert(IntBackend backend) throws GeException;
    public native boolean backendDeleteById(String id) throws GeException;
    public native IntBackend[] backendSelect(String tail) throws GeException;
    public native IntBackend[] backendSelectById(String id) throws GeException;
    
    public native IntCall[] callSelectByBackend(String backend, String tail) throws GeException;

    public native IntFile[] fileSelectByBackendDirectory(String backend, String directory, String tail) throws GeException;
    public native IntFile[] fileSelectByBackendDirectoryModifiedBetween(String backend, String directory, long modified1, long modified2, String tail) throws GeException;

    public native IntPassword[] passwordSelectByBackendName(String backend, String name) throws GeException;
}
