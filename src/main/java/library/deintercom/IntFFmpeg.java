package library.deintercom;

import android.os.Handler;
import android.os.Looper;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class IntFFmpeg extends LjlObject {
    public static native double duration(String file) throws GeException;
    public static native String lyrics(String file) throws GeException;

    // ConvertPcm

    public static native boolean convertPcm(String input, String output, int rate, double from, double to, String lyrics) throws GeException;

    public interface ConvertPcmAsyncCallback {
        void callback(GeException exception);
    }

    public static void convertPcmAsync(String input, String output, int rate, double from, double to, String lyrics, ConvertPcmAsyncCallback callback) {
        new Thread(() -> {
            try {
                convertPcm(input, output, rate, from, to, lyrics);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }

    // ConvertPcmData

    public static native boolean convertPcmData(short[] input, int n, String output, int rate, double from, double to, String lyrics) throws GeException;

    public interface ConvertPcmDataAsyncCallback {
        void callback(GeException exception);
    }

    public static void convertPcmDataAsync(short[] input, int n, String output, int rate, double from, double to, String lyrics, ConvertPcmDataAsyncCallback callback) {
        new Thread(() -> {
            try {
                convertPcmData(input, n, output, rate, from, to, lyrics);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }
}
