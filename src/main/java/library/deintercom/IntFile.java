package library.deintercom;

import library.java.lang.LjlObject;

public class IntFile extends LjlObject {
    public String backend;
    public String directory;
    public String name;
    public long modified;
    public double duration;
}
