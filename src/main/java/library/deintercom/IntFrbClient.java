package library.deintercom;

import android.os.Handler;
import android.os.Looper;
import java.io.Closeable;
import java.util.ArrayList;
import library.firebaseext.FrbCredential;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class IntFrbClient extends LjlObject implements Closeable {
    public interface Listener {
        void pushDone(IntFrbClient sender, String name, GeException exception);
    }

    public static class Listeners extends ArrayList<Listener> implements Listener {
        @Override
        public void pushDone(IntFrbClient sender, String name, GeException exception) {
            for (Listener listener : this) {
                listener.pushDone(sender, name, exception);
            }
        }
    }

    private long object;
    private long weak;
    public String base;
    public FrbCredential credential;
    public Listeners listeners;

    @Override
    protected void finalize() throws Throwable {
        close();
    }

    @Override
    public native void close();

    public void pushDone(String name, GeException exception) {
        new Handler(Looper.getMainLooper()).post(() -> listeners.pushDone(this, name, exception));
    }

    public static native IntFrbClient client(String base, FrbCredential credential);

    public native void abort();

    // PushCall

    public native void pushCall(IntPushCall pushCall);

    // PushIP

    public native void pushIP(IntPushIP pushIP);

    // PushAccept

    public native void pushAccept(IntPushAccept pushAccept);
}
