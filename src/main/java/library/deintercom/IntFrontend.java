package library.deintercom;

import library.java.lang.LjlObject;

public class IntFrontend extends LjlObject {
    public String backend;
    public String id;
    public int os;
    public String project;
    public String token;
    public boolean online;
    public long lastSeen;
}
