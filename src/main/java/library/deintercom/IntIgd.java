package library.deintercom;

import android.os.Handler;
import android.os.Looper;
import java.io.Closeable;
import java.util.ArrayList;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class IntIgd extends LjlObject implements Closeable {
    public interface Listener {
        void ip(IntIgd sender, String local, String external);
        void error(IntIgd sender, GeException exception);
    }

    public static class Listeners extends ArrayList<Listener> implements Listener {
        @Override
        public void ip(IntIgd sender, String local, String external) {
            for (Listener listener : this) {
                listener.ip(sender, local, external);
            }
        }

        @Override
        public void error(IntIgd sender, GeException exception) {
            for (Listener listener : this) {
                listener.error(sender, exception);
            }
        }
    }

    private long object;
    private long weak;
    public int http;
    public int sip;
    public String local;
    public String external;
    public Listeners listeners;

    @Override
    protected void finalize() throws Throwable {
        close();
    }

    @Override
    public native void close();

    public void ip(String local, String external) {
        new Handler(Looper.getMainLooper()).post(() -> listeners.ip(this, local, external));
    }

    public void error(GeException exception) {
        new Handler(Looper.getMainLooper()).post(() -> listeners.error(this, exception));
    }

    public static native IntIgd igd(String iface, int http, int sip);
}
