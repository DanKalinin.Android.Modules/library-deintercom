package library.deintercom;

import android.content.Context;
import androidx.startup.Initializer;
import java.util.ArrayList;
import java.util.List;
import library.apnext.ApnInit;
import library.asteriskext.AstrInit;
import library.avahiext.AvhInit;
import library.avrdudeext.AvdInit;
import library.curlext.CrlInit;
import library.fdroidext.FdrInit;
import library.ffmpegext.MpgInit;
import library.firebaseext.FrbInit;
import library.glibext.GeInit;
import library.glibnetworkingext.GneInit;
import library.gnutlsext.TlsInit;
import library.gssdpext.SsdpInit;
import library.gupnpext.UpnpInit;
import library.gupnpigdext.IgdInit;
import library.java.lang.LjlObject;
import library.jsonext.JseInit;
import library.jwt2ext.Jwt2Init;
import library.niceext.IceInit;
import library.pjsipext.PjeInit;
import library.ptyext.PtyInit;
import library.soupext.SoeInit;
import library.sqliteext.SqleInit;
import library.usbext.UsbeInit;
import library.voskext.VskInit;

public class IntInit extends LjlObject implements Initializer<Void> {
    @Override
    public Void create(Context context) {
        System.loadLibrary("library-deintercom");
        return null;
    }

    @Override
    public List<Class<? extends Initializer<?>>> dependencies() {
        ArrayList<Class<? extends Initializer<?>>> ret = new ArrayList<>();
        ret.add(GeInit.class);
        ret.add(SoeInit.class);
        ret.add(TlsInit.class);
        ret.add(GneInit.class);
        ret.add(JseInit.class);
        ret.add(AvhInit.class);
        ret.add(SqleInit.class);
        ret.add(PjeInit.class);
        ret.add(PtyInit.class);
        ret.add(UsbeInit.class);
        ret.add(FrbInit.class);
        ret.add(CrlInit.class);
        ret.add(ApnInit.class);
        ret.add(Jwt2Init.class);
        ret.add(SsdpInit.class);
        ret.add(UpnpInit.class);
        ret.add(IgdInit.class);
        ret.add(IceInit.class);
        ret.add(AstrInit.class);
        ret.add(MpgInit.class);
        ret.add(FdrInit.class);
        ret.add(AvdInit.class);
        ret.add(VskInit.class);
        return ret;
    }
}
