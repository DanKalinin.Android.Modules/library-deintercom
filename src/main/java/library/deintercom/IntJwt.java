package library.deintercom;

import library.glibext.GeException;
import library.java.lang.LjlObject;

public class IntJwt extends LjlObject {
    public static native String qrCodeEncode(IntQrCode qrCode, String key) throws GeException;
    public static native IntQrCode qrCodeDecode(String token, String key) throws GeException;
}
