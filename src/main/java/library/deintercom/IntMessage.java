package library.deintercom;

import library.java.lang.LjlObject;

public class IntMessage extends LjlObject {
    public boolean right;
    public long timestamp;
    public IntText text;
    public IntCall call;
    public IntFile file;
    public IntAction[] actions;
}
