package library.deintercom;

import library.java.lang.LjlObject;

public class IntPassword extends LjlObject {
    public String backend;
    public String name;
    public long modified;
    public double duration;
    public boolean enabled;
}
