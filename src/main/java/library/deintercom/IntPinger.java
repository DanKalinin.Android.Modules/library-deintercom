package library.deintercom;

import android.os.Handler;
import android.os.Looper;
import java.io.Closeable;
import java.util.ArrayList;
import library.java.lang.LjlObject;

public class IntPinger extends LjlObject implements Closeable {
    public interface Listener {
        void pong(IntPinger sender, IntClient current);
    }

    public static class Listeners extends ArrayList<Listener> implements Listener {
        @Override
        public void pong(IntPinger sender, IntClient current) {
            for (Listener listener : this) {
                listener.pong(sender, current);
            }
        }
    }

    private long object;
    private long weak;
    public IntClient local;
    public IntClient external;
    public IntClient current;
    public Listeners listeners;

    @Override
    protected void finalize() throws Throwable {
        close();
    }

    @Override
    public native void close();

    public void pong(IntClient current) {
        new Handler(Looper.getMainLooper()).post(() -> listeners.pong(this, current));
    }

    public static native IntPinger pinger(IntClient local, IntClient external, IntClient current);

    public native void ping();
}
