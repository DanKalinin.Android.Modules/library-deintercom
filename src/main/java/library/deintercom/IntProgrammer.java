package library.deintercom;

import android.os.Handler;
import android.os.Looper;
import java.io.Closeable;
import java.util.ArrayList;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class IntProgrammer extends LjlObject implements Closeable {
    public interface Listener {
        void progress(IntProgrammer sender, int percent, double time, String header, int finish);
    }

    public static class Listeners extends ArrayList<Listener> implements Listener {
        @Override
        public void progress(IntProgrammer sender, int percent, double time, String header, int finish) {
            for (Listener listener : this) {
                listener.progress(sender, percent, time, header, finish);
            }
        }
    }

    private long object;
    private long weak;
    public Listeners listeners;

    @Override
    protected void finalize() throws Throwable {
        close();
    }

    @Override
    public native void close();

    public void progress(int percent, double time, String header, int finish) {
        new Handler(Looper.getMainLooper()).post(() -> listeners.progress(this, percent, time, header, finish));
    }

    public static native IntProgrammer programmer();

    // Update

    public native boolean updateSync(String config, String firmware, String port) throws GeException;

    public interface UpdateAsyncCallback {
        void callback(GeException exception);
    }

    public void updateAsync(String config, String firmware, String port, UpdateAsyncCallback callback) {
        new Thread(() -> {
            try {
                updateSync(config, firmware, port);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }
}
