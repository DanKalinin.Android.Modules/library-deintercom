package library.deintercom;

import android.os.Handler;
import android.os.Looper;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class IntPtyFs extends LjlObject {
    public static native boolean mountSync(String directory) throws GeException;

    public interface MountAsyncCallback {
        void callback(GeException exception);
    }

    public static void mountAsync(String directory, MountAsyncCallback callback) {
        new Thread(() -> {
            try {
                mountSync(directory);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }
}
