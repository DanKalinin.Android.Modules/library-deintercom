package library.deintercom;

import library.java.lang.LjlObject;

public class IntPushIP extends LjlObject {
    public String project;
    public String token;
    public String id;
    public String local;
    public String external;
}
