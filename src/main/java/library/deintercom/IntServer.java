package library.deintercom;

import java.io.Closeable;
import library.asteriskext.AstrDatabase;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class IntServer extends LjlObject implements Closeable {
    private long object;
    public int port;
    public IntDatabase database;
    public IntUsbSerial serial;
    public IntFrbClient frbClient;
    public IntApnClient apnClient;
    public IntIgd igd;
    public AstrDatabase astrDatabase;
    public IntSipAgent agent;

    @Override
    protected void finalize() throws Throwable {
        close();
    }

    @Override
    public native void close();

    public static native IntServer server(int port, IntDatabase database, IntUsbSerial serial, IntFrbClient frbClient, IntApnClient apnClient, IntIgd igd, AstrDatabase astrDatabase, IntSipAgent agent) throws GeException;
}
