package library.deintercom;

import java.io.Closeable;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class IntSipAgent extends LjlObject implements Closeable {
    private long object;
    public String device;

    @Override
    protected void finalize() throws Throwable {
        close();
    }

    @Override
    public native void close();

    public static native IntSipAgent agent(String device, IntTone tone) throws GeException;

    public native int accountAdd(String host, String username, String password, boolean stun) throws GeException;
    public native boolean accountDelete(int account) throws GeException;

    public native int callMake(int account, String host, String extension) throws GeException;
    public native boolean callHangup(int call) throws GeException;
    public native boolean callHangupIncoming() throws GeException;

    public native boolean audDevRefresh() throws GeException;
}
