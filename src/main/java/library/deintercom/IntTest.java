package library.deintercom;

import library.java.lang.LjlObject;

public class IntTest extends LjlObject {
    public static native void test();
    public static native int fork();
}
