package library.deintercom;

import android.os.Handler;
import android.os.Looper;
import java.io.Closeable;
import java.util.ArrayList;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class IntUsbSerial extends LjlObject implements Closeable {
    public interface Listener {
        void call(IntUsbSerial sender, byte a);
    }

    public static class Listeners extends ArrayList<Listener> implements Listener {
        @Override
        public void call(IntUsbSerial sender, byte a) {
            for (Listener listener : this) {
                listener.call(sender, a);
            }
        }
    }

    private long object;
    private long weak;
    public Listeners listeners;

    @Override
    protected void finalize() throws Throwable {
        close();
    }

    @Override
    public native void close();

    public void call(byte a) {
        new Handler(Looper.getMainLooper()).post(() -> listeners.call(this, a));
    }

    public static native IntUsbSerial serial(int fd) throws GeException;
    public native boolean answer(byte a) throws GeException;
    public native boolean open(byte a) throws GeException;

    // Read

    public native boolean readSync() throws GeException;

    public interface ReadAsyncCallback {
        void callback(GeException exception);
    }

    public void readAsync(ReadAsyncCallback callback) {
        new Thread(() -> {
            try {
                readSync();
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }
}
